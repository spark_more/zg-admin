package com.grace.mail.domain;

public enum MailVisibleEnums {
    // 保存|未发送
    SAVE,
    // 已经发送
    SEND,
    // 待发送
    WAIT,
    // 发送失败
    FAIL

}
