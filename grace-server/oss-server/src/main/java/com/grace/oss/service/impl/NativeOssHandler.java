package com.grace.oss.service.impl;

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import com.grace.common.exception.file.FileSizeLimitExceededException;
import com.grace.common.exception.file.InvalidExtensionException;
import com.grace.common.utils.file.FileUploadUtils;
import com.grace.oss.domain.OlyOss;
import com.grace.oss.enums.OlyStageRoot;
import com.grace.oss.enums.OssEnum;
import com.grace.oss.mapper.OssServerMapper;
import com.grace.oss.properties.NativeProperties;
import com.grace.oss.properties.OssConfigProperties;
import com.grace.oss.service.OssHandler;
import com.grace.oss.utils.FileTypes;
import com.grace.oss.utils.OssUtils;
import com.grace.system.service.impl.SysSearchConfigServiceImpl;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import net.coobird.thumbnailator.Thumbnails;

/**
 * 储存在本地
 */
@Service
public class NativeOssHandler implements OssHandler {
    /**
     * 缩略图前缀
     */
    public final static String THUMBNAIL_PREFIX = "thumbnail_";

    @Autowired
    private SysSearchConfigServiceImpl configService;

    @Autowired
    private OssServerMapper ossMapper;

    private static final Logger log = LoggerFactory.getLogger(NativeOssHandler.class);

    @Override
    public OlyOss ossUpload(MultipartFile file)
            throws IOException, FileSizeLimitExceededException, InvalidExtensionException {

        OssHandler.check(file, configService);
        // 设置返回结果
        OlyOss data = new OlyOss();
        // 验证支持文件后缀
        // 验证支持类型

        // 二级目录
        String fileType = OssUtils.getFileTypeExtension(FileUploadUtils.getExtension(file));
        // 构造文件key
        String key = OssHandler.getKey(fileType, file, Boolean.parseBoolean(
                configService.selectConfigDefaultValue(OssConfigProperties.OSS_CONFIG_GROUP.defaultValue(),
                        OssConfigProperties.OSS_FILE_NAME_ZH_PY)));
        // 文件上传
        FileUploadUtils.upload(OlyStageRoot.UPLOAD_DIR.getRoot(key), file);
        data.setFileName(FilenameUtils.getName(file.getOriginalFilename()));
        data.setFileType(fileType);
        data.setSize(file.getSize());
        data.setFk(OssHandler.pathToUrl(key));
        data.setOssType(getOssType());
        // 判断是否是图片
        if ("image".equals(fileType)) {
            String thumbnailKey = Paths
                    .get(FilenameUtils.getFullPathNoEndSeparator(key), THUMBNAIL_PREFIX + FilenameUtils.getName(key))
                    .toString();
            String thumbnailPath = OlyStageRoot.UPLOAD_DIR.getRoot(thumbnailKey);
            BufferedImage bufferedImage = ImageIO
                    .read(new FileInputStream(OlyStageRoot.UPLOAD_DIR.getRoot(key).toString()));
            Thumbnails.of(bufferedImage).size(THUMB_WIDTH, THUMB_HEIGHT).toFile(thumbnailPath.toString());
        }
        data.setDomain(configService.selectConfigValueByGk(OssConfigProperties.OSS_CONFIG_GROUP.getValue(),
                NativeProperties.OSS_DOMAIN.getValue()));
        ossMapper.insertOlyOss(data);
        return data;
    }

    @Override
    public void ossDelete(String key) {
        Path path = Paths.get(OlyStageRoot.UPLOAD_DIR.getRoot(key));
        try {
            Files.deleteIfExists(path);
        } catch (IOException e1) {
            log.error(key + "删除失败,原因本地可能不存在", e1);
        }
        if (FileTypes.getImg(FilenameUtils.getExtension(key))) {
            try {
                Files.deleteIfExists(Paths.get(OlyStageRoot.UPLOAD_DIR.getRoot(""),
                        FilenameUtils.getFullPathNoEndSeparator(key), THUMBNAIL_PREFIX + FilenameUtils.getName(key)));
            } catch (IOException e) {
                log.error(key + "附件删除失败,原因本地可能不存在", e);
            }
        }
        ossMapper.deleteOlyOssByFk(OssHandler.pathToUrl(key));
    }

    @Override
    public OlyOss ossAppointUpload(MultipartFile file, OlyStageRoot rootPath, String fileName) throws IOException {
        // 直接上传 fileName即为key
        if (StringUtils.isNotEmpty(fileName)) {
            FileUploadUtils.upload(rootPath.getRoot(fileName), file);
        } else {
            // 时间路径
            fileName = OssHandler.getKey("", file, true);
            FileUploadUtils.upload(rootPath.getRoot(fileName), file);
        }
        OlyOss olyOss = new OlyOss();
        olyOss.setDomain(configService.selectConfigValueByGk(OssConfigProperties.OSS_CONFIG_GROUP.getValue(),
                NativeProperties.OSS_DOMAIN.getValue()));
        olyOss.setFk(OssHandler.pathToUrl(fileName));
        return olyOss;
    }

    @Override
    public OlyOss ossAppointUpload(MultipartFile file, OlyStageRoot rootPath) throws IOException {
        return ossAppointUpload(file, rootPath, FilenameUtils.getName(file.getOriginalFilename()));
    }

    @Override
    public String getOssType() {
        return OssEnum.OSS_TYPE_NATIVE.getValue();
    }

}
