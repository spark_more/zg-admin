package com.grace.oss.enums;

public enum MimeTypeEnum {
    IMAGE(new String[] { "bmp", "gif", "jpg", "jpeg", "png" }, 2048);

    private final String[] prefix;

    private final Integer size;

    private MimeTypeEnum(String[] prefix, Integer size) {
        this.prefix = prefix;
        this.size = size;
    }

    public String[] getPrefix() {
        return prefix;
    }

    public Integer getSize() {
        return size;
    }

}
