
<h4 align="center">基于SpringBoot+Vue3的内容管理系统</h4>

## 系统说明

本系统基于若依前后端分离二次开发的内容管理系统。
* 后台前端采用Vue3、Element Plus。
* 后端采用Spring Boot、Spring Security、Redis 、权限认证使用Jwt,支持多终端认证系统。
* 用户端后端采用Spring Boot、Spring Security、Redis、Thymeleaf,传统Cookie。
* 支持加载动态权限菜单,多方式轻松权限控制。
* 高效率开发，使用代码生成器可以一键生成前后端代码。
* 多站点：支持后台统一站点管理多站点
* 多主题：同一站点支持多主题,同时后台通过简单配置,也可以实现群站效果。
* 栏目：前端菜单管理,支持多级树,可以应付复杂的导航,已支持额外SEO字段。
* 分类：支持多级树,完全可以应付复杂分类,已支持额外SEO字段。
* 标签：标签用于标记文章特色,已支持额外SEO字段。
* 文章：支持不同类型,特色推荐,多关键字的文章发布,支持额外SEO字段。
* 储存：储存方面统一接口,文件可视化管理,方便后续的维护以及扩展。
* 邮件：邮件支持在线配置,发送,支持多种类型邮件发送,支持定时发送。
* 链接：支持分组,方便做导航类网站。
* 文章：包含评论,浏览记录,评分,收藏等。
* 反馈: 支持不同类型反馈,方便搜集用户在使用中存在的问题。

## 结构说明
ZG-ADMIN
├─bin
├─doc
├─grace-admin 后台启动入口
├─grace-common 通用部分
├─grace-flyway 管理数据库
├─grace-framework 框架核心
├─grace-generator  代码生成
├─grace-mybatis    操作数据库
├─grace-oly   oly聚合
│  └─oly-cms   内容聚合
│      ├─cms-admin  内容管理
│      ├─cms-comment 评论
│      ├─cms-common  内容通用工具
│      ├─cms-general 内容通用入口
│      ├─cms-hander  内容控制
│      └─cms-web  内容前台启动入口
├─grace-quartz  定时任务
├─grace-redis    redis缓存
├─grace-server    系统服务
│  ├─mail-server  邮件服务
│  └─oss-server   储存服务
├─grace-system    系统访问
│  ├─system-core  系统代码
│  ├─system-current  通用部分
│  └─system-user  用户相关
├─sql

## 支持
- 一般网站
- 带用户可评论网站
- 论坛
- 小程序后台

## 系统启动
- 启动之前请确保安装Redis以及Mysql
- 分离后台位于grace-admin模块下的GraceApplication.java
- 分离后台地址[zg-vue3](https://gitee.com/Getawy/zg-vue3)
- 前台不分离位于grace-oly/oly-cms/cms-web模块下的ZhiGeWebApplication.java