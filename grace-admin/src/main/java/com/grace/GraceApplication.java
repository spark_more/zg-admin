package com.grace;

import java.io.File;
import java.nio.file.Paths;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import com.grace.common.config.GraceConfig;
import com.grace.common.utils.YamlUtil;
import com.grace.oss.enums.OlyStageRoot;

/**
 * 服务启动入口
 * 启动之前请确保安装redis
 * 
 * @author grace
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
// 基本扫描路径
@ComponentScan(basePackages = { "com.grace", "com.oly" })
public class GraceApplication {
    static {
        // 覆盖默认文件配置位置
        File ymlFile = Paths.get(GraceConfig.getUserWork(), "config", "application.yml").toFile();
        // 工作路径
        String workPath = null;
        // 站点唯一编码
        String onlyCode = null;
        boolean appointProfile;
        if (ymlFile.exists() && ymlFile.isFile()) {
            workPath = YamlUtil.yamlPropertiesByFile("grace.profile", ymlFile).toString();
            onlyCode = YamlUtil.yamlPropertiesByFile("grace.onlyCode", ymlFile).toString();
            appointProfile = "true"
                    .equals(YamlUtil.yamlPropertiesByFile("grace.appointProfile", ymlFile).toString());
            // 覆盖默认配置如果默认当前jar下config目录下的话,这里可以不加
            System.setProperty("spring.config.additional-location", "file:" + ymlFile.getParent() + File.separator);
        } // 默认配置
        else {
            workPath = YamlUtil.yamlPropertiesByResources("grace.profile", "application.yml").toString();
            onlyCode = YamlUtil.yamlPropertiesByResources("grace.onlyCode", "application.yml").toString();
            appointProfile = "true"
                    .equals(
                            YamlUtil.yamlPropertiesByResources("grace.appointProfile", "application.yml").toString());
        }
        // 自定义工作路径
        if (!appointProfile) {
            workPath = Paths.get(GraceConfig.getUserWork(), workPath).toString();
        }
        // 日志路径
        System.setProperty("log_path", Paths.get(workPath, OlyStageRoot.LOGS_DIR.getValue(), onlyCode).toString());
        // 本地文件上传临时目录
        System.setProperty("tmp_path", Paths.get(workPath, OlyStageRoot.TMP_DIR.getValue(), onlyCode).toString());
    }

    public static void main(String[] args) {
        SpringApplication.run(GraceApplication.class, args);
        System.out.println(
                "ZZZZZZZZZZZZZZZZZZZ        GGGGGGGGGGGGG\n"
                        + "Z:::::::::::::::::Z     GGG::::::::::::G\n"
                        + "Z:::::::::::::::::Z   GG:::::::::::::::G\n"
                        + "Z:::ZZZZZZZZ:::::Z   G:::::GGGGGGGG::::G\n"
                        + "ZZZZZ     Z:::::Z   G:::::G       GGGGGG\n"
                        + "        Z:::::Z    G:::::G              \n"
                        + "   止  Z:::::Z     G:::::G      戈       \n"
                        + "      Z:::::Z      G:::::G    GGGGGGGGGG\n"
                        + "     Z:::::Z       G:::::G    G::::::::G\n"
                        + "    Z:::::Z        G:::::G    GGGGG::::G\n"
                        + "Z:::::Z        Z:::::::::G        G::::G\n"
                        + "ZZZ:::::Z     ZZZZZ G:::::G       G::::G\n"
                        + "Z::::::ZZZZZZZZ:::Z  G:::::GGGGGGGG::::G\n"
                        + "Z:::::::::::::::::Z   GG:::::::::::::::G\n"
                        + "Z:::::::::::::::::Z     GGG::::::GGG:::G\n"
                        + "ZZZZZZZZZZZZZZZ  启动成功 GGGGGG    GGGG");
    }
}
