package com.grace.web.controller.tool;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class A {
    public static void main(String[] args) throws ClientProtocolException, IOException {

        CloseableHttpClient httpClient = HttpClients.createDefault();

        HttpGet httpGet = new HttpGet("http://localhost:8090/web/theme/getThemeSetting/zgblog");

        CloseableHttpResponse response = httpClient.execute(httpGet);

        try {

            System.out.println(EntityUtils.toString(response.getEntity(), "utf-8"));

        } finally {

            response.close();

        }

    }

}
