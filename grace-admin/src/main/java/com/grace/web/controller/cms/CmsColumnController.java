package com.grace.web.controller.cms;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oly.cms.admin.service.ICmsColumnService;
import com.oly.cms.common.domain.entity.CmsColumn;
import com.grace.common.annotation.Log;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.enums.BusinessType;

import com.grace.common.utils.poi.ExcelUtil;

/**
 * 内容菜单Controller
 * 
 * @author grace
 * @date 2022-11-19
 */
@RestController
@RequestMapping("/cms/column")
public class CmsColumnController extends BaseController {
    @Autowired
    private ICmsColumnService cmsColumnService;

    /**
     * 查询内容菜单列表
     */
    @PreAuthorize("@ss.hasPermi('cms:column:list')")
    @GetMapping("/list")
    public AjaxResult list(CmsColumn cmsColumn) {
        List<CmsColumn> list = cmsColumnService.listCmsColumn(cmsColumn);
        return success(list);
    }

    /**
     * 导出内容菜单列表
     */
    @PreAuthorize("@ss.hasPermi('cms:column:export')")
    @Log(title = "内容菜单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsColumn cmsColumn) {
        List<CmsColumn> list = cmsColumnService.listCmsColumn(cmsColumn);
        ExcelUtil<CmsColumn> util = new ExcelUtil<CmsColumn>(CmsColumn.class);
        util.exportExcel(response, list, "内容菜单数据");
    }

    /**
     * 获取内容菜单详细信息
     */
    @PreAuthorize("@ss.hasPermi('cms:column:query')")
    @GetMapping(value = "/{columnId}")
    public AjaxResult getInfo(@PathVariable("columnId") Long columnId) {
        return success(cmsColumnService.selectCmsColumnById(columnId));
    }

    /**
     * 新增内容菜单
     */
    @PreAuthorize("@ss.hasPermi('cms:column:add')")
    @Log(title = "内容菜单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsColumn cmsColumn) {
        cmsColumn.setCreateBy(getUsername());
        return toAjax(cmsColumnService.insertCmsColumn(cmsColumn));
    }

    /**
     * 修改内容菜单
     */
    @PreAuthorize("@ss.hasPermi('cms:column:edit')")
    @Log(title = "内容菜单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsColumn cmsColumn) {
        cmsColumn.setUpdateBy(getUsername());
        return toAjax(cmsColumnService.updateCmsColumn(cmsColumn));
    }

    /**
     * 删除内容菜单
     */
    @PreAuthorize("@ss.hasPermi('cms:column:remove')")
    @Log(title = "内容菜单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{columnId}")
    public AjaxResult remove(@PathVariable Long columnId) {
        return toAjax(cmsColumnService.deleteCmsColumnById(columnId));
    }
}
