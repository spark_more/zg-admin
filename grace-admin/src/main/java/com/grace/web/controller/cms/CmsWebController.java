package com.grace.web.controller.cms;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oly.cms.admin.service.ICmsWebService;
import com.oly.cms.common.domain.entity.CmsWeb;
import com.grace.common.annotation.Log;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.core.page.TableDataInfo;
import com.grace.common.enums.BusinessType;
import com.grace.common.utils.poi.ExcelUtil;

/**
 * 站点Controller
 * 
 * @author zhiGe
 * @date 2022-12-06
 */
@RestController
@RequestMapping("/cms/web")
public class CmsWebController extends BaseController {
    @Autowired
    private ICmsWebService cmsWebService;

    /**
     * 查询站点列表
     */
    @PreAuthorize("@ss.hasPermi('cms:web:list')")
    @GetMapping("/list")
    public TableDataInfo list(CmsWeb cmsWeb) {
        startPage();
        List<CmsWeb> list = cmsWebService.selectCmsWebList(cmsWeb);
        return getDataTable(list);
    }

    /**
     * 导出站点列表
     */
    @PreAuthorize("@ss.hasPermi('cms:web:export')")
    @Log(title = "站点", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsWeb cmsWeb) {
        List<CmsWeb> list = cmsWebService.selectCmsWebList(cmsWeb);
        ExcelUtil<CmsWeb> util = new ExcelUtil<CmsWeb>(CmsWeb.class);
        util.exportExcel(response, list, "站点数据");
    }

    /**
     * 获取站点详细信息
     */
    @PreAuthorize("@ss.hasPermi('cms:web:query')")
    @GetMapping(value = "/{webName}")
    public AjaxResult getInfo(@PathVariable("webName") String webName) {
        return success(cmsWebService.selectCmsWebByWebName(webName));
    }

    /**
     * 新增站点
     */
    @PreAuthorize("@ss.hasPermi('cms:web:add')")
    @Log(title = "站点", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody @Validated CmsWeb cmsWeb) {
        cmsWeb.setCreateBy(getUsername());
        return toAjax(cmsWebService.insertCmsWeb(cmsWeb));
    }

    /**
     * 修改站点
     */
    @PreAuthorize("@ss.hasPermi('cms:web:edit')")
    @Log(title = "站点", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody @Validated CmsWeb cmsWeb) {
        cmsWeb.setUpdateBy(getUsername());
        return toAjax(cmsWebService.updateCmsWeb(cmsWeb));
    }

    /**
     * 删除站点
     */
    @PreAuthorize("@ss.hasPermi('cms:web:remove')")
    @Log(title = "站点", businessType = BusinessType.DELETE)
    @DeleteMapping("/{webNames}")
    public AjaxResult remove(@PathVariable String[] webNames) {
        return toAjax(cmsWebService.deleteCmsWebByWebNames(webNames));
    }
}
