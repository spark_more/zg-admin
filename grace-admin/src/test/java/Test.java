import org.springframework.util.AntPathMatcher;

public class Test {
    public static void main(String[] args) {
       AntPathMatcher antPathMatcher=new AntPathMatcher();
       System.out.println(antPathMatcher.match("/a/*/a", "/a/b/a"));
       System.out.println(antPathMatcher.match("/a/*/a", "/a/a"));
    }
}
