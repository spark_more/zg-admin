package com.oly.cms.web.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.oly.cms.common.domain.entity.CmsTheme;
import com.oly.cms.common.model.support.ThemeTreeNode;

public interface IWebThemeService {
    /**
     * 删除主题 通过主题名字
     * 
     * @param themeName
     * @return
     * @throws FileNotFoundException
     */
    int deleteByName(String themeName) throws FileNotFoundException;

    /**
     * 获取主题配置及主题信息
     * 
     * @param themeName
     * @return
     */
    Object selectThemeConfigSetting(String themeName);

    /**
     * 获取主题配置表单
     * 
     * @param themeName
     * @return
     */
    Object selectThemeConfigForm(String themeName);

    /**
     * 获取已经启用的主题
     * 
     * @return
     */
    CmsTheme selectThemeByUsed();

    /**
     * 
     * @param file  文件
     * @param cover 覆盖上传
     * @return
     * @throws IOException
     * @throws Throwable
     */
    void uploadTheme(MultipartFile file, boolean cover) throws Throwable, IOException;

    List<ThemeTreeNode> selectThemeFileTree(String path);

    /**
     * 获取启用的主题
     * 
     * @return
     */
    String selectUseTheme() throws FileNotFoundException;

}
