package com.oly.cms.web.controller.page;

import javax.servlet.http.HttpServletResponse;

import com.oly.cms.common.constant.OlySystemConstant;
import com.oly.cms.general.annotation.WebLog;
import com.oly.cms.general.model.enums.WebLogType;
import com.oly.cms.query.model.param.ArticleSearchParam;
import com.oly.cms.query.model.param.CategorySearchParam;
import com.oly.cms.web.service.page.WebPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * 前端试图控制
 */
@Controller
@RequestMapping(OlySystemConstant.REQUEST_PREFIX)
public class WebPageController {
    @Autowired
    private WebPageService webPageService;

    /**
     * 内容主页
     *
     * @param mp
     * @return
     */
    @WebLog(title = "主页请求", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}")
    public String index(@PathVariable( "themeName") String themeName,
            @PathVariable( "pageNum") Integer pageNum, ModelMap mp) {
        if (pageNum == null) {
            pageNum = 1;
            mp.put("pageNum", pageNum);
        }
        return webPageService.index(themeName, mp);
    }

    /**
     * 请求指定文章
     *
     * @param articleId
     * @param mp
     * @return
     */
    @WebLog(title = "获取文章", logType = WebLogType.PAGE)
    @GetMapping(value = { "/{themeName}/article/{articleId}" })
    public String post(@PathVariable( "articleId") Long articleId,
            @PathVariable( "themeName") String themeName,
            ModelMap mp) {
        return webPageService.selectArticleById(themeName, articleId, mp);
    }

    /**
     * 文章列表
     *
     * @param themeName
     * @param mp
     * @param parm
     * @return
     */
     @WebLog(title = "获取文章列表", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}/article")
    public String listArticle(@PathVariable( "themeName") String themeName, ModelMap mp,
            ArticleSearchParam parm) {
        return webPageService.listWebArticle(themeName, mp, parm);
    }

    /**
     * 分类列表
     *
     * @param themeName
     * @param cat
     * @param mp
     * @return
     */
    @WebLog(title = "获取分类列表", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}/category/{categoryType}")
    public String cats(@PathVariable("themeName") String themeName,@PathVariable("categoryType") String categoryType, CategorySearchParam param,
            ModelMap mp) {
        return webPageService.listCmsCategory(themeName, param, mp);

    }

    /**
     * 分类详情
     *
     * @param themeName
     * @param catId
     * @param mp
     * @return
     */
    @WebLog(title = "获取分类", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}/category/{categoryId}" )
    public String category(@PathVariable( "themeName") String themeName,
            @PathVariable("categoryId") Long catId, ModelMap mp) {
        return webPageService.selectCmsCategoryById(themeName, catId, mp);
    }

    /**
     * 友情链接
     *
     * @param themeName
     * @param mp
     * @return
     */
    @WebLog(title = "友情链接", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}/links")
    public String links(@PathVariable("themeName") String themeName, ModelMap mp) {
        return webPageService.links(themeName, mp);
    }

    /**
     * 关于本站
     *
     * @param themeName
     * @param mp
     * @return
     */
    // @WebLog(title = "关于本站", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}/about" )
    public String about(@PathVariable( "themeName") String themeName, ModelMap mp) {
        return webPageService.about(themeName, mp);
    }

    /**
     * 排行榜
     *
     * @param themeName
     * @param modelMap
     * @return
     */
    @WebLog(title = "排行榜", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}/rank")
    public String rank(@PathVariable("themeName") String themeName, ModelMap modelMap) {
        return webPageService.rank(themeName, modelMap);
    }

    /**
     * 时间线
     *
     * @param themeName
     * @param modelMap
     * @param pageNum
     * @param pageSize
     * @return
     */
    @WebLog(title = "时间线", logType = WebLogType.PAGE)
    @GetMapping(value = { "/timeLine", "/timeLine/page/{pageNum}/{pageSize}",
            "/{themeName}/timeLine",
            "/{themeName}/timeLine/page/{pageNum}/{pageSize}" })
    public String timeLine(@PathVariable( "themeName") String themeName,
            ModelMap modelMap,
            @PathVariable( "pageNum") Integer pageNum,
            @PathVariable( "pageSize") Integer pageSize) {
        return webPageService.timeLine(themeName, modelMap, pageNum, pageSize);
    }

    /**
     * 反馈
     *
     * @param themeName
     * @param mp
     * @return
     */
    @WebLog(title = "反馈", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}/contact")
    public String contact(@PathVariable("themeName") String themeName, ModelMap mp) {
        return webPageService.contact(themeName, mp);
    }

    /**
     * 联盟页面
     *
     * @param themeName
     * @param mp
     * @return
     */
    @WebLog(title = "联盟页", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}/union")
    public String Union(@PathVariable( "themeName") String themeName, ModelMap mp) {
        return webPageService.union(themeName, mp);
    }

    /**
     * 自定义页面
     *
     * @param page
     * @return
     */
    @WebLog(title = "自定义页面", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}/custom/{page}")
    public String customPage(@PathVariable("themeName") String themeName, ModelMap mp,
            @PathVariable("page") String page) {
        return webPageService.fr(themeName, mp, page);
    }

    @GetMapping("/{themeName}/robots.txt" )
    public void robots(@PathVariable("themeName") String themeName,
            HttpServletResponse response, ModelMap mp) {
        webPageService.robots(themeName, response, mp);
    }

    @GetMapping( "/{themeName}/sitemap.xml")
    public void siteMapIndex(@PathVariable( "themeName") String themeName,
            HttpServletResponse response, ModelMap mp) {
        webPageService.siteMapIndex(themeName, response, mp);
    }

    @GetMapping("/site/{themeName}/{file:.+}")
    public void siteMap(@PathVariable( "themeName") String themeName,
            @PathVariable( "file") String fileName,
            HttpServletResponse response, ModelMap mp) {
        webPageService.siteMap(themeName, fileName, response, mp);
    }

}
