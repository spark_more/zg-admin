package com.oly.cms.web.controller;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.oly.cms.web.service.impl.WebThemeServiceImpl;
import com.oly.cms.web.utils.WebUtils;
import com.grace.common.core.domain.AjaxResult;

@RestController
@RequestMapping("/web/theme")
public class WebThemeController extends CommonController {

    @Autowired
    private WebThemeServiceImpl themeService;

    /**
     * 主题文件上传
     * 
     * @param file
     * @param cover
     * @param request
     * @return
     * @throws IOException
     */
    @PostMapping("/uploadTheme")
    public AjaxResult uploadTheme(@RequestParam("themeFile") MultipartFile file, boolean cover,
            HttpServletRequest request) throws IOException {
        themeService.uploadTheme(file, cover);
        return AjaxResult.success();
    }

    /**
     * 通过主题获取信息及配置
     * 
     * @param themeName
     * @return
     * @throws FileNotFoundException
     */
    @GetMapping("/getThemeSetting/{themeName}")
    public AjaxResult selectThemeSetting(@PathVariable("themeName") String themeName) {
        return AjaxResult.success(themeService.selectThemeConfigSetting(themeName));
    }

    /**
     * 通过主题获取信息及配置
     * 
     * @param themeName
     * @return
     * @throws FileNotFoundException
     */
    @GetMapping("/getThemeConfigForm/{themeName}")
    public AjaxResult selectThemeConfig(@PathVariable("themeName") String themeName) {
        return AjaxResult.success(themeService.selectThemeConfigForm(themeName));
    }

    @PostMapping("/deleteTheme")
    public AjaxResult deleteTheme(String themeName) throws FileNotFoundException {
        WebUtils.deleteThemeFile(themeName);
        return AjaxResult.success();
    }

    /**
     * 获取主题文件树
     * 
     * @param themeName
     * @return
     */
    @GetMapping(value = { "/getThemeList", "/getThemeList/{themeName}" })
    public AjaxResult selectThemeList(@PathVariable("themeName") String themeName) {
        return AjaxResult.success(themeService.selectThemeFileTree(themeName));
    }

}
