package com.oly.cms.web;

import java.io.File;
import java.nio.file.Paths;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import com.grace.common.config.GraceConfig;
import com.grace.common.utils.YamlUtil;
import com.grace.oss.enums.OlyStageRoot;

/**
 * 止戈前端启动程序
 * 
 * @author zhiGe
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@ComponentScan(basePackages = { "com.grace", "com.oly" })
public class ZhiGeWebApplication {

        static {
                // 覆盖默认文件配置位置
                File ymlFile = Paths.get(GraceConfig.getUserWork(), "webConfig", "application.yml").toFile();
                String workPath = null;
                String onlyCode = null;
                boolean appointProfile;

                if (ymlFile.exists() && ymlFile.isFile()) {
                        workPath = YamlUtil.yamlPropertiesByFile("grace.profile", ymlFile).toString();
                        onlyCode = YamlUtil.yamlPropertiesByFile("grace.onlyCode", ymlFile).toString();
                        appointProfile = "true"
                                        .equals(YamlUtil.yamlPropertiesByFile("grace.appointProfile", ymlFile)
                                                        .toString());
                        // 覆盖默认配置
                        System.setProperty("spring.config.additional-location",
                                        "file:" + ymlFile.getParent() + File.separator);
                } // 默认配置
                else {
                        workPath = YamlUtil.yamlPropertiesByResources("grace.profile", "application.yml").toString();
                        onlyCode = YamlUtil.yamlPropertiesByResources("grace.onlyCode", "application.yml").toString();
                        appointProfile = "true".equals(YamlUtil.yamlPropertiesByResources("grace.appointProfile",
                                        "application.yml").toString());
                }
                // 自定义工作路径
                if (!appointProfile) {
                        workPath = Paths.get(GraceConfig.getUserWork(), workPath).toString();
                }
                // 日志路径
                System.setProperty("log_path",
                                Paths.get(workPath, OlyStageRoot.LOGS_DIR.getValue(), onlyCode).toString());
                // 本地文件上传临时目录
                System.setProperty("tmp_path",
                                Paths.get(workPath, OlyStageRoot.TMP_DIR.getValue(), onlyCode).toString());
        }

        public static void main(String[] args) {
                // System.setProperty("spring.devtools.restart.enabled", "false");
                SpringApplication.run(ZhiGeWebApplication.class, args);
                System.out.println("(♥◠‿◠)ﾉﾞ  若依启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                                " .-------.       ____     __        \n" +
                                " |  _ _   \\      \\   \\   /  /    \n" +
                                " | ( ' )  |       \\  _. /  '       \n" +
                                " |(_ o _) /        _( )_ .'         \n" +
                                " | (_,_).' __  ___(_ o _)'          \n" +
                                " |  |\\ \\  |  ||   |(_,_)'         \n" +
                                " |  | \\ `'   /|   `-'  /           \n" +
                                " |  |  \\    /  \\      /           \n" +
                                " ''-'   `'-'    `-..-'              ");

        }

}
