package com.oly.cms.web.config;

import com.oly.cms.web.interceptor.IPInterceptor;
import com.grace.oss.enums.OlyStageRoot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

        @Autowired
        private IPInterceptor ipInterceptor;

        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
                String path = ("file:" + OlyStageRoot.THEME_DIR.getOnlyCodeRoot("") + "/").replace("\\", "/");
                /** 主题静态资源配置 */
                registry.addResourceHandler("/themes/**").addResourceLocations("classpath:/templates/themes/")
                                .addResourceLocations(path);
                /** 静态资源映射 */
                registry.addResourceHandler("/server/oss/download/**").addResourceLocations(
                                ("file:" + OlyStageRoot.UPLOAD_DIR.getRoot("") + "/").replace("\\", "/"));
        }

        /**
         * 自定义拦截规则
         */
        @Override
        public void addInterceptors(InterceptorRegistry registry) {
                // 指定某些管理IP才能访问
                registry.addInterceptor(ipInterceptor).addPathPatterns("/web/theme/**");
        }

}