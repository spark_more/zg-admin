package com.oly.cms.admin.mapper;

import java.util.List;

import com.oly.cms.common.domain.entity.CmsCategory;

public interface CmsCategoryMapper {
	/**
	 * 添加类目
	 * 
	 * @param param
	 * @return
	 */
	int insertCmsCategory(CmsCategory param);

	/**
	 * 修改类目
	 * 
	 * @param param
	 * @return
	 */
	int updateCmsCategory(CmsCategory param);

	/**
	 * 删除单个分类
	 * 
	 * @param categoryId
	 * @return
	 */
	int deleteCmsCategoryById(Long categoryId);

	/**
	 * 更新次级列表
	 * 
	 * @param param
	 * @return
	 */
	int updateChildCategoryAncestors(CmsCategory param);

	/**
	 * 通过父类id获取 子节点id
	 * @param parentId
	 * @return
	 */
	List<Long> listCategoryIdsByParentId(Long parentId);

	/**
	 * 统计关联节点
	 * 
	 * @param categoryId
	 * @return
	 */
	int countArticleByCategoryId(Long categoryId);

	/**
	 * @param vp
	 * @return
	 */
	int updateChildCategoryVisible(CmsCategory vp);

}