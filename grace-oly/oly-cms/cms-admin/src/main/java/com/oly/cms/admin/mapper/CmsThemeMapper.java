package com.oly.cms.admin.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.oly.cms.common.domain.entity.CmsTheme;

/**
 * 主题Mapper接口
 * 
 * @author zhiGe
 * @date 2022-12-06
 */
public interface CmsThemeMapper {

    /**
     * 查询主题
     * webName->w
     * themeName->t
     * 
     * @param cmsTheme
     * @return 主题
     */
    public CmsTheme selectCmsThemeByWt(CmsTheme cmsTheme);

    /**
     * 查询主题列表 通过站点名字
     * 
     * @param webName
     * @return 主题集合
     */
    public List<CmsTheme> listCmsTheme(String webName);

    /**
     * 新增主题
     * 
     * @param cmsTheme 主题
     * @return 结果
     */
    public int insertCmsTheme(CmsTheme cmsTheme);

    /**
     * 修改主题
     * 
     * @param cmsTheme 主题
     * @return 结果
     */
    public int updateCmsTheme(CmsTheme cmsTheme);

    /**
     * 删除主题
     * 
     * @param cmsTheme
     * @return 结果
     */
    public int deleteCmsThemeByTn(CmsTheme cmsTheme);

    /**
     * 分类关联主题
     * 
     * @param themeName
     * @param categoryId
     * @return
     */
    public int categoryUnionTheme(@Param("themeName") String themeName, @Param("categoryId") Long categoryId);

}
