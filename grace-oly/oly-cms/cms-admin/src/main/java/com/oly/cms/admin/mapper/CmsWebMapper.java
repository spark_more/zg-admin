package com.oly.cms.admin.mapper;

import java.util.List;

import com.oly.cms.common.domain.entity.CmsWeb;

/**
 * 站点Mapper接口
 * 
 * @author zhiGe
 * @date 2022-12-06
 */
public interface CmsWebMapper {
    /**
     * 查询站点
     * 
     * @param webName 站点名
     * @return 站点
     */
    public CmsWeb selectCmsWebByWebName(String webName);

    /**
     * 查询站点列表
     * 
     * @param cmsWeb 站点
     * @return 站点集合
     */
    public List<CmsWeb> selectCmsWebList(CmsWeb cmsWeb);

    /**
     * 新增站点
     * 
     * @param cmsWeb 站点
     * @return 结果
     */
    public int insertCmsWeb(CmsWeb cmsWeb);

    /**
     * 修改站点
     * 
     * @param cmsWeb 站点
     * @return 结果
     */
    public int updateCmsWeb(CmsWeb cmsWeb);

    /**
     * 删除站点
     * 
     * @param webName 站点名字
     * @return 结果
     */
    public int deleteCmsWebByWebName(String webName);

    /**
     * 批量删除站点
     * 
     * @param webNames 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsWebByWebNames(String[] webNames);
}
