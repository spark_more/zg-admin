package com.oly.cms.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.oly.cms.admin.mapper.CmsArticleLiquidMapper;
import com.oly.cms.admin.mapper.CmsThemeMapper;
import com.oly.cms.admin.service.ICmsThemeService;
import com.oly.cms.common.domain.entity.CmsTheme;
import com.grace.common.utils.DateUtils;

/**
 * 主题Service业务层处理
 * 
 * @author zhiGe
 * @date 2022-12-06
 */
@Service
public class CmsThemeServiceImpl implements ICmsThemeService {
    @Autowired
    private CmsThemeMapper cmsThemeMapper;

    @Autowired
    private CmsArticleLiquidMapper cmsArticleLiquidMapper;

    /**
     * 查询主题
     * 
     * @param cmsTheme
     * @return 主题
     */
    @Override
    public CmsTheme selectCmsThemeByWt(CmsTheme cmsTheme) {
        return cmsThemeMapper.selectCmsThemeByWt(cmsTheme);
    }

    /**
     * 查询主题列表
     * 
     * @param webName
     * @return 主题
     */
    @Override
    public List<CmsTheme> listCmsTheme(String webName) {
        return cmsThemeMapper.listCmsTheme(webName);
    }

    /**
     * 新增主题
     * 
     * @param cmsTheme 主题
     * @return 结果
     */
    @Override
    public int insertCmsTheme(CmsTheme cmsTheme) {
        cmsTheme.setCreateTime(DateUtils.getNowDate());
        return cmsThemeMapper.insertCmsTheme(cmsTheme);
    }

    /**
     * 修改主题
     * 
     * @param cmsTheme 主题
     * @return 结果
     */
    @Override
    public int updateCmsTheme(CmsTheme cmsTheme) {
        return cmsThemeMapper.updateCmsTheme(cmsTheme);
    }

    /**
     * 删除主题信息
     * 
     * @param cmsTheme
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteCmsThemeByTn(CmsTheme cmsTheme) {
        return cmsThemeMapper.deleteCmsThemeByTn(cmsTheme);
    }

    @Override
    public int categoryUnionTheme(String themeName, Long categoryId) {
        return cmsThemeMapper.categoryUnionTheme(themeName, categoryId);
    }

}
