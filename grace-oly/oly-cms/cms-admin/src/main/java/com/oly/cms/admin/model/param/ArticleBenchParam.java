package com.oly.cms.admin.model.param;

import java.io.Serializable;

public class ArticleBenchParam implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long[] articleIds;

    private String updateBy;

    private Integer visible;

    private Integer articleTop;

    public Long[] getArticleIds() {
        return articleIds;
    }

    public void setArticleIds(Long[] articleIds) {
        this.articleIds = articleIds;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public Integer getArticleTop() {
        return articleTop;
    }

    public void setArticleTop(Integer articleTop) {
        this.articleTop = articleTop;
    }

}
