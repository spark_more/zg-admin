package com.oly.cms.comment.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.oly.cms.comment.mapper.CmsCommentHandMapper;
import com.oly.cms.comment.mapper.CmsCommentMapper;
import com.oly.cms.comment.model.CmsComment;
import com.oly.cms.comment.model.enums.CommentTypeEnum;
import com.oly.cms.comment.model.enums.CommentVisibleEnums;
import com.oly.cms.comment.model.vo.CmsCommentVo;
import com.oly.cms.comment.service.ICmsCommentService;
import com.oly.cms.common.constant.CmsCacheConstant;
import com.oly.cms.common.event.CacheWebRefreshAllEvent;
import com.grace.common.exception.ServiceException;

/**
 * 评论Service业务层处理
 * 
 * @author zd
 * @date 2022-07-06
 */
@Service
public class CmsCommentServiceImpl implements ICmsCommentService {
    @Autowired
    private CmsCommentMapper cmsCommentMapper;

    @Autowired
    private CmsCommentHandMapper cmsCommentHandMapper;

    @Autowired
    private ApplicationEventPublisher app;

    /**
     * 查询评论
     * 
     * @param id 评论主键
     * @return 评论
     */
    @Override
    public CmsCommentVo selectCmsCommentVoById(Long id) {
        return cmsCommentMapper.selectCmsCommentById(id);
    }

    /**
     * 查询评论列表
     * 
     * @param cmsComment 评论
     * @return 评论
     */
    @Override
    public List<CmsCommentVo> listCmsCommentVo(CmsComment cmsComment) {
        return cmsCommentMapper.listCmsCommentVo(cmsComment);
    }

    /**
     * 新增评论
     * 
     * @param cmsComment 评论
     * @return 结果
     */
    @Override
    @Transactional
    public int insertCmsComment(CmsComment cmsComment) {
        int re = cmsCommentMapper.insertCmsComment(cmsComment);
        if (CommentVisibleEnums.PASS.ordinal() == cmsComment.getVisible().intValue()) {
            if (cmsComment.getCommentType() == CommentTypeEnum.ARTICLE.ordinal()) {
                cmsCommentMapper.updateCmsArticleCountByDiscuss(Long.parseLong(cmsComment.getTypeId()));
            }
            app.publishEvent(new CacheWebRefreshAllEvent(this, CmsCacheConstant.COMMENT_CACHE_KEY_PREFIX));
        }
        return re;
    }

    /**
     * 修改评论
     * 
     * @param cmsComment 评论
     * @return 结果
     */
    @Override
    public int updateCmsComment(CmsComment cmsComment) {
        int re = cmsCommentMapper.updateCmsComment(cmsComment);
        app.publishEvent(new CacheWebRefreshAllEvent(this, CmsCacheConstant.COMMENT_CACHE_KEY_PREFIX));
        return re;
    }

    /**
     * 批量删除评论
     * 
     * @param commentIds 需要删除的评论主键
     * @return 结果
     */
    @Override
    public int deleteCmsCommentByCommentIds(Long[] commentIds) {
        int re = 0;
        for (long id : commentIds) {
            this.deleteCmsCommentByCommentId(id);
            re += 1;
        }
        return re;
    }

    /**
     * 删除评论信息
     * 
     * @param commentId 评论主键
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteCmsCommentByCommentId(Long commentId) {
        cmsCommentHandMapper.deleteCmsCommentHandByCommentId(commentId);
        return cmsCommentMapper.deleteCmsCommentByCommentId(commentId);
    }

 
    @Override
    public List<CmsCommentVo> listCmsCommentVoParent(CmsComment cmsComment) {
        cmsComment.setParentId(0L);
        return this.listCmsCommentVo(cmsComment);
    }

    @Override
    public List<CmsCommentVo> listCmsCommentVoChild(CmsComment cmsComment) {
        if (cmsComment.getParentId() == null || cmsComment.getParentId() == 0L) {
            throw new ServiceException("父级ID不能为O或者空");
        }
        return this.listCmsCommentVo(cmsComment);
    }

    @Override
    public int batchCmsComment(Long[] ids, Integer visible, Long orderNum, String remark, String updateBy) {
        CmsComment cmsComment = new CmsComment();
        cmsComment.setVisible(visible);
        cmsComment.setOrderNum(orderNum);
        cmsComment.setRemark(remark);
        cmsComment.setUpdateBy(updateBy);
        for (Long id : ids) {
            cmsComment.setCommentId(id);
            cmsCommentMapper.updateCmsComment(cmsComment);
            if (visible != null) {
                cmsCommentMapper.updateCmsArticleCountByDiscuss(id);
            }
        }
        app.publishEvent(new CacheWebRefreshAllEvent(this, CmsCacheConstant.COMMENT_CACHE_KEY_PREFIX));
        return ids.length;
    }
}
