package com.oly.cms.query.mapper;

import java.util.List;

import com.oly.cms.common.domain.VisibleParam;
import com.oly.cms.common.domain.entity.CmsCategory;
import com.oly.cms.query.model.param.CategorySearchParam;

public interface CategorySearchMapper {
    /**
     * 获取类别列表
     * 
     * 1.通过主题关联分类获取
     * 2.通过主题
     * 
     * 
     * @param param
     * @return
     */
    List<CmsCategory> listCmsCategory(CategorySearchParam param);

    /**
     * 获取分类信息
     * 
     * @param param
     * @return
     */
    CmsCategory selectCmsCategoryById(CategorySearchParam param);

    /**
     * 获取分类信息
     * 
     * @param categoryUrl
     * @return
     */
    CmsCategory selectCmsCategoryByUrl(String categoryUrl);

    /**
     * 如果路径不为空，路径不允许重复
     * 同级目录下分类名不能重复
     * 
     * @param param
     * @return
     */
    CmsCategory checkCategoryUnique(CmsCategory param);

    /**
     * 通过文章id获取文章关联分类列表 类型为分类
     * 
     * @param vParam
     * @return
     */
    List<CmsCategory> listArticleCategoryByArticleId(VisibleParam vParam);

        /**
     * 通过文章id获取文章关联的分类列表 类型为标签
     * 
     * @param vParam
     * @return
     */
    List<CmsCategory> listArticleTagByArticleId(VisibleParam vParam);

    int selectCategoryNum(String types);
    
    /**
     * 获取列别列表通过给定的ID
     * @param categoryIds
     * @return
     */
    List<CmsCategory> listCmsCategoryByIds(Long[] categoryIds);

}
