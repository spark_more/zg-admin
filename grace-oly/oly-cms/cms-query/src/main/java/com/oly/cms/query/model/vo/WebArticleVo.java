package com.oly.cms.query.model.vo;

import java.util.List;

import com.oly.cms.common.domain.entity.CmsArticle;
import com.oly.cms.common.domain.entity.CmsArticleCount;
import com.oly.cms.common.domain.entity.CmsCategory;
import com.oly.cms.common.domain.vo.UserVo;

/** 所有文章字段 */
public class WebArticleVo extends CmsArticle {

    private static final long serialVersionUID = 653673311404721257L;

    private List<CmsCategory> categories;

    private List<CmsCategory> tags;

    private CmsArticleCount cmsArticleCount;

    private UserVo createUser;

    public CmsArticleCount getCmsArticleCount() {
        return cmsArticleCount;
    }

    public void setCmsArticleCount(CmsArticleCount cmsArticleCount) {
        this.cmsArticleCount = cmsArticleCount;
    }

    public List<CmsCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<CmsCategory> categories) {
        this.categories = categories;
    }

    public UserVo getCreateUser() {
        return createUser;
    }

    public void setCreateUser(UserVo createUser) {
        this.createUser = createUser;
    }

    public List<CmsCategory> getTags() {
        return tags;
    }

    public void setTags(List<CmsCategory> tags) {
        this.tags = tags;
    }

    

}
