package com.oly.cms.query.mapper;

import java.util.List;

import com.oly.cms.common.domain.entity.CmsArticle;
import com.oly.cms.common.model.TimeNum;
import com.oly.cms.query.model.param.ArticleSearchParam;
import com.oly.cms.query.model.vo.WebArticleVo;

public interface ArticleSearchMapper {
    
    /**
     * 获取文章通过ID
     * @param articleSearchParam
     * @return
     */
    WebArticleVo selectArticleById(ArticleSearchParam articleSearchParam);
    
     /**
     * 获取文章通过Url
     * @param articleSearchParam
     * @return
     */
    WebArticleVo selectArticleByUrl(ArticleSearchParam articleSearchParam);

    /**
     * 获取文章列表
     * 
     * @param webArticleSearchParam
     * @return
     */
    List<WebArticleVo> listArticleBySearch(ArticleSearchParam webArticleSearchParam);
    
    /**
     * 获取文章ID列表
     * @param categoryId
     * @return
     */
    List<Long> listArticleIdsByCategoryId(Long categoryId);
    

    boolean allowComment(Long postId);

    int selectArticleNumUnion(String themeName);

    int selectArticleNum();

    List<TimeNum> listArticleTimeNum();

}
