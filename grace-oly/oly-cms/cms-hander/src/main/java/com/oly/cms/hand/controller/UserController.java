package com.oly.cms.hand.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.oly.cms.common.constant.OlySystemConstant;

@Controller
@RequestMapping(OlySystemConstant.REQUEST_PREFIX)
public class UserController {
     
    
    @GetMapping("{themeName}/login")
    public String login() {
        return "user/login";
    }

    @GetMapping("{themeName}/register")
    public String register() {
        return "user/register";
    }

    @GetMapping("{themeName}/personal/{userId}")
    public String personal(@PathVariable("userId") Long userId) {
        return "user/personal";
    }

}
