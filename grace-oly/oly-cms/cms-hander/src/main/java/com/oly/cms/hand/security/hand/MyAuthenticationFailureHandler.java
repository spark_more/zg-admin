package com.oly.cms.hand.security.hand;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson2.JSONObject;
import com.grace.common.core.domain.AjaxResult;

/**
 * 登录失败自定义处理类
 * 
 * @author 止戈
 */
@Component
public class MyAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException exception) throws IOException, ServletException {
        if (UnauthorizedEntryPoint.isAjaxRequest(request)) {
            response.setHeader("Content-type", "text/html;charset=UTF-8");
            response.getWriter().write(JSONObject.toJSONString(AjaxResult.error(exception.getMessage())));
            response.getWriter().flush();
            response.getWriter().close();
        } else {
            response.sendRedirect("/login?error=true&msg=" + URLEncoder.encode(exception.getMessage(), "UTF-8"));
        }

    }
}