package com.oly.cms.hand.service.impl;

import com.oly.cms.hand.mapper.UserHandleMapper;

import com.oly.cms.hand.service.IHandleService;
import com.grace.common.core.domain.entity.SysUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HandleServiceImpl implements IHandleService {
    @Autowired
    private UserHandleMapper webHandleMapper;

    @Override
    public SysUser selectUserById(Long userId) {
        return webHandleMapper.selectUserById(userId);
    }

}
