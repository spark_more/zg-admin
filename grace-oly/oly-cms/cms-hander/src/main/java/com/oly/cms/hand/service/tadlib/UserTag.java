package com.oly.cms.hand.service.tadlib;

import com.grace.common.core.domain.entity.SysUser;
import com.grace.common.core.domain.entity.SysUserInfo;
import com.grace.common.utils.SecurityUtils;

import org.springframework.stereotype.Service;

@Service("userTag")
public class UserTag {

    /**
     * 获取登陆用户
     * 
     * @return
     */
    public SysUser getLoginUser() {

        return SecurityUtils.getLoginUser().getUser();
    }

    public SysUserInfo getLoginUserInfo() {
        return null;
    }

    public SysUser getUserByUserId(Long userId) {
        return null;
    }

    public SysUser getUserByUserLoginName(String userName) {
        return null;
    }

}
