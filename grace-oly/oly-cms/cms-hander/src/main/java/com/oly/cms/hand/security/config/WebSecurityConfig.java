package com.oly.cms.hand.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.oly.cms.hand.security.CustomAuthenticationDetailsSource;
import com.oly.cms.hand.security.CustomAuthenticationProvider;
import com.oly.cms.hand.security.hand.MyAuthenticationFailureHandler;
import com.oly.cms.hand.security.hand.MyAuthenticationSuccessHandler;
import com.oly.cms.hand.security.hand.RedisTokenRepositoryImpl;
import com.oly.cms.hand.security.hand.UnauthorizedEntryPoint;
import com.oly.cms.hand.security.service.WebUserDetailsService;

/**
 * spring security配置
 * 
 * @author grace
 */
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 自定义用户认证逻辑
     */
    @Autowired
    private WebUserDetailsService userDetailsService;

    @Autowired
    private CustomAuthenticationDetailsSource authenticationDetailsSource;

    @Autowired
    private CustomAuthenticationProvider authenticationProvider;

    @Autowired
    private MyAuthenticationFailureHandler failureHandler;

    @Autowired
    private MyAuthenticationSuccessHandler successHandler;

    @Autowired
    private RedisTokenRepositoryImpl repository;

    /**
     * 解决 无法直接注入 AuthenticationManager
     *
     * @return
     * @throws Exception
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    // /**
    // * anyRequest | 匹配所有请求路径
    // * access | SpringEl表达式结果为true时可以访问
    // * anonymous | 匿名可以访问
    // * denyAll | 用户不能访问
    // * fullyAuthenticated | 用户完全认证可以访问（非remember-me下自动登录）
    // * hasAnyAuthority | 如果有参数，参数表示权限，则其中任何一个权限可以访问
    // * hasAnyRole | 如果有参数，参数表示角色，则其中任何一个角色可以访问
    // * hasAuthority | 如果有参数，参数表示权限，则其权限可以访问
    // * hasIpAddress | 如果有参数，参数表示IP地址，如果用户IP和参数匹配，则可以访问
    // * hasRole | 如果有参数，参数表示角色，则其角色可以访问
    // * permitAll | 用户可以任意访问
    // * rememberMe | 允许通过remember-me登录的用户访问
    // * authenticated | 用户登录后可访问
    // */
    // @Override
    // protected void configure(HttpSecurity httpSecurity) throws Exception {
    // // 注解标记允许匿名访问的url
    // //
    // ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry
    // // registry = httpSecurity
    // // .authorizeRequests();

    // httpSecurity
    // // CSRF禁用，因为不使用session
    // .csrf().disable()
    // // 基于token，所以不需要session
    // .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS).and()
    // .formLogin() // 基于 Form 表单登录验证
    // .loginPage("/login").successForwardUrl("/index").failureUrl("/login?error=true").and()
    // // 过滤请求
    // .authorizeRequests()
    // // 允许匿名访问,登录以后不许访问
    // .antMatchers("/login").anonymous()
    // // 静态资源，可匿名访问
    // .antMatchers(HttpMethod.GET, "/**", "/*.html", "/**/*.html", "/**/*.css",
    // "/**/*.js", "/profile/**",
    // "/server/oss/download/**")
    // .permitAll()
    // .antMatchers("/swagger-ui.html", "/swagger-resources/**", "/webjars/**",
    // "/*/api-docs", "/druid/**")
    // .permitAll()
    // .antMatchers(HttpMethod.POST, "/web/hand/user/**")
    // .permitAll()
    // // 除上面外的所有请求全部需要鉴权认证
    // .anyRequest().authenticated()
    // .and()
    // .headers().frameOptions().disable();
    // }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 添加认证异常入口
        http.exceptionHandling().authenticationEntryPoint(new UnauthorizedEntryPoint())
                .and()
                .authorizeRequests()
                // 获取验证码允许匿名访问
                .antMatchers(HttpMethod.GET, "/**", "/libs/**", "/css/**", "/js/**", "/images/**", "/api/**")
                .permitAll()
                .antMatchers(HttpMethod.POST, "/web/hand/user/register")
                .permitAll()
                .antMatchers(HttpMethod.POST, "/web/**").authenticated()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login").permitAll().loginProcessingUrl("/login")
                .failureHandler(failureHandler)
                .successHandler(successHandler)
                .authenticationDetailsSource(authenticationDetailsSource).and();
        // 启用记住密码
        http.authorizeRequests()
                .and()
                .rememberMe().tokenValiditySeconds(5 * 24 * 60 * 60).tokenRepository(repository)
                .userDetailsService(userDetailsService);

        http.csrf().disable();
    }

    /**
     * 强散列哈希加密实现
     */
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * 身份认证接口
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
        auth.authenticationProvider(authenticationProvider);
    }

}
