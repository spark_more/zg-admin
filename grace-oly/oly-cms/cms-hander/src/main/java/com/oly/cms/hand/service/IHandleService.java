package com.oly.cms.hand.service;

import com.grace.common.core.domain.entity.SysUser;

public interface IHandleService {

    /**
     * 通过用户ID查询用户
     * 
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public SysUser selectUserById(Long userId);

}
