package com.oly.cms.hand.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oly.cms.common.constant.OlySystemConstant;
import com.oly.cms.hand.security.service.WebRegisterService;
import com.oly.cms.hand.service.IHandleService;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.core.domain.model.RegisterBody;
import com.grace.common.enums.SysConfigGroups;
import com.grace.common.utils.SecurityUtils;
import com.grace.common.utils.StringUtils;
import com.grace.system.service.impl.SysSearchConfigServiceImpl;

@Controller
@RequestMapping(OlySystemConstant.REQUEST_PREFIX)
public class UserHandController extends BaseController {

    @Autowired
    private IHandleService webHandleService;

    @Autowired
    private WebRegisterService registerService;

    @Autowired
    private SysSearchConfigServiceImpl configService;

    @GetMapping("/isLogin/{userId}")
    public @ResponseBody AjaxResult isLogin(@PathVariable("userId") Long userId) {
        Long loginUserId = SecurityUtils.getUserId();
        if (loginUserId == null) {
            return AjaxResult.error("用户未登录");
        } else if (!loginUserId.equals(userId)) {
            return AjaxResult.error("登陆账号不符");
        } else
            return AjaxResult.success("身份验证成功");
    }

    @GetMapping("/getUser/{userId}")
    public AjaxResult selectUser(@PathVariable("userId") Long userId) {
        return AjaxResult.success(webHandleService.selectUserById(userId));
    }

    /**
     * 用户注册 
     * @param user
     * @return
     */
    @PostMapping("/register")
    @ResponseBody
    public AjaxResult register(RegisterBody user) {
        System.out.println(user);
        if (!("true".equals(configService.selectConfigValueByGk(SysConfigGroups.SYS_CONFIG.getValue(),
                "sys.account.registerUser")))) {
            return AjaxResult.error("当前系统没有开启注册功能！");
        }
        String msg = registerService.register(user);
        return StringUtils.isEmpty(msg) ? success() : error(msg);
    }

}
