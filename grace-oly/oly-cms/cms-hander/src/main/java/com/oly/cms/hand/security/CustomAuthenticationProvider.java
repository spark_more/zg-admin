package com.oly.cms.hand.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.oly.cms.general.manager.WebAsyncManager;
import com.oly.cms.hand.manager.factory.HandAsyncFactory;
import com.oly.cms.hand.security.context.AuthenticationContextHolder;
import com.oly.cms.hand.security.service.WebUserDetailsService;
import com.grace.common.constant.CacheConstants;
import com.grace.common.constant.Constants;
import com.grace.common.enums.SysConfigGroups;
import com.grace.common.utils.MessageUtils;
import com.grace.common.utils.StringUtils;
import com.grace.redis.utils.RedisCache;
import com.grace.system.service.impl.SysSearchConfigServiceImpl;

@Component
public class CustomAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
    @Autowired
    private WebUserDetailsService userDetailsService;

    @Autowired
    private SysSearchConfigServiceImpl configService;

    @Autowired
    private RedisCache redisCache;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails,
            UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

        // 获取登录提交的验证码
        CustomWebAuthenticationDetails details = (CustomWebAuthenticationDetails) authentication.getDetails();

        String validateCode = details.getValidateCode();

        String validateUuid = details.getValidateUuid();

        boolean captchaEnabled = configService.selectCaptchaEnabled(SysConfigGroups.SYS_CONFIG.getValue());
        // 验证码开关
        if (captchaEnabled) {
            validateCaptcha(userDetails.getUsername(), validateCode, validateUuid);
        }

    }

    @Override
    protected UserDetails retrieveUser(String username,
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
        AuthenticationContextHolder.setContext(usernamePasswordAuthenticationToken);
        return userDetailsService.loadUserByUsername(username);
    }

    /**
     * 校验验证码
     * 
     * @param username 用户名
     * @param code     验证码
     * @param uuid     唯一标识
     * @return 结果
     */
    public void validateCaptcha(String username, String code, String uuid) {
        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY + StringUtils.nvl(uuid, "");
        String captcha = redisCache.getCacheObject(verifyKey);
        redisCache.deleteObject(verifyKey);
        if (captcha == null) {
            WebAsyncManager.me().execute(HandAsyncFactory.recordLogininfor(username, "admin",Constants.LOGIN_FAIL,
                    MessageUtils.message("user.jcaptcha.expire")));
            throw new AccountExpiredException("验证码已经过期");
        }
        if (!code.equalsIgnoreCase(captcha)) {
            WebAsyncManager.me().execute(HandAsyncFactory.recordLogininfor(username,"admin", Constants.LOGIN_FAIL,
                    MessageUtils.message("user.jcaptcha.error")));
            throw new CredentialsExpiredException("验证码错误");
        }
    }

}
