package com.oly.cms.general.service.search;

import java.util.List;

import com.oly.cms.common.domain.entity.CmsCategory;
import com.oly.cms.common.enums.CategoryNodeTypeEnums;
import com.oly.cms.common.enums.CommonVisibleEnums;
import com.oly.cms.common.model.properties.OlyWebConfigProperties;
import com.oly.cms.general.service.IGeneralSearchService;
import com.oly.cms.general.utils.tree.CategoryTreeUtils;
import com.oly.cms.query.mapper.CategorySearchMapper;
import com.oly.cms.query.model.param.CategorySearchParam;
import com.grace.common.utils.StringUtils;
import com.grace.system.service.impl.SysSearchConfigServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GeneralCategoryServiceImpl implements IGeneralSearchService {
    @Autowired
    private CategorySearchMapper categorySearchMapper;

    @Autowired
    private SysSearchConfigServiceImpl configService;

    public CmsCategory selectCmsCategoryById(CategorySearchParam param) {

        return categorySearchMapper.selectCmsCategoryById(param);
    }

    /**
     * 获取分类列表
     * 
     * @param param
     * @return
     */
    public List<CmsCategory> listCmsCategory(CategorySearchParam param) {
        param.setVisible(CommonVisibleEnums.SHOW.ordinal());
        setSupportType(param, param.getSearchValue());
        return categorySearchMapper.listCmsCategory(param);
    }

    public CmsCategory selectCmsCategoryTree(CategorySearchParam param) {
        if (param == null || param.getCategoryId() == null) {
            param = new CategorySearchParam();
            param.setCategoryId(0L);
        }
        return CategoryTreeUtils.getCategoryTree(this.listCmsCategory(param), param.getCategoryId());
    }

   /**
    * 
    * @param orderNum
    * @param categoryId
    * @param nodeType
    * @param themeName
    * @return
    */
    public List<CmsCategory> listCmsCategory(Long orderNum,CategoryNodeTypeEnums nodeType,Long categoryId, 
            String themeName) {
        CategorySearchParam param = new CategorySearchParam();
        param.setOrderNum(orderNum);
        param.setCategoryId(categoryId);
        param.setNodeType(nodeType.ordinal());
        param.setThemeName(themeName);
        return this.listCmsCategory(param);
    }

    /**
     * 
     * @param orderNum
     * @param categoryId
     * @param nodeType
     * @param themeName
     * @return
     */
    public CmsCategory selectCmsCategoryTree(Long orderNum,CategoryNodeTypeEnums nodeType,Long categoryId,
            String themeName) {
        return CategoryTreeUtils.getCategoryTree(this.listCmsCategory(orderNum,nodeType, categoryId, themeName),
                categoryId == null ? 0L : categoryId);
    }

    public int selectCategoryNum(String themeName) {
        String supportType = configService.selectConfigDefaultValue(themeName, OlyWebConfigProperties.ARTICLE_TYPES);
        return categorySearchMapper.selectCategoryNum(supportType);
    }

    private void setSupportType(CmsCategory param, String themeName) {
        if (StringUtils.isEmpty(themeName)) {
            return;
        }
        String supportType = configService.selectConfigDefaultValue(themeName, OlyWebConfigProperties.ARTICLE_TYPES);
        if (StringUtils.isNotEmpty(supportType)) {
            param.getParams().put("supportType", supportType);
        }
    }
    
    /**
     * 确定主题知否支持当前类型
     * @param param
     * @return
     */
    public boolean checkSupportCategory(CategorySearchParam param) {
        CmsCategory category = categorySearchMapper.selectCmsCategoryById(param);
        if (category != null) {
          return true;
        }
        return false;
    }

}
