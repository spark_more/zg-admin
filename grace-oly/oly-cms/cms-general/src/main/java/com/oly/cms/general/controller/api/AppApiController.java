package com.oly.cms.general.controller.api;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grace.common.core.domain.AjaxResult;
import com.grace.system.service.impl.SysSearchConfigServiceImpl;
import com.oly.cms.common.constant.OlySystemConstant;

/*
 *@description: 获取相关配置获取
 *@author: zg
 *@date: 2021-07-02 16:53:01
*/
@RestController
@RequestMapping(OlySystemConstant.REQUEST_PREFIX+"/*/api/app")
@CrossOrigin
public class AppApiController {
    @Autowired
    private SysSearchConfigServiceImpl configService;

    /**
     * 获取App配置
     * 
     * @return
     */
    @GetMapping("/getAppInfo")
    public AjaxResult getAppInfo() {
        Map<String, String> bMap = configService.selectConfigValueMapByGroupName("");
        return AjaxResult.success(bMap);
    }

}
