package com.oly.cms.general.service.search;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.oly.cms.common.model.TimeNum;
import com.oly.cms.common.model.enums.OrderEnums;
import com.oly.cms.common.model.properties.OlyWebConfigProperties;
import com.oly.cms.general.model.PageArticleTimeLine;
import com.oly.cms.general.service.IGeneralSearchService;
import com.oly.cms.query.mapper.ArticleSearchMapper;
import com.oly.cms.query.model.param.ArticleSearchParam;
import com.oly.cms.query.model.vo.WebArticleVo;
import com.grace.common.utils.DateUtils;
import com.grace.common.utils.StringUtils;
import com.grace.system.service.impl.SysSearchConfigServiceImpl;

@Service
public class GeneralArticleServiceImpl implements IGeneralSearchService {

    @Autowired
    private ArticleSearchMapper webSearchMapper;

    @Autowired
    private SysSearchConfigServiceImpl configService;

    /**
     * 综合查询
     * 
     * @param bb
     * @return
     */
    public List<WebArticleVo> listArticleBySearch(ArticleSearchParam bb) {

        return webSearchMapper.listArticleBySearch(bb);
    }

    /**
     * 
     * @param articleId
     * @param useMd
     * @param useContent
     * @return
     */
    public WebArticleVo selectArticleById(Long articleId,String themeName,Boolean useMd,Boolean useContent) {
        ArticleSearchParam articleSearchParam=new ArticleSearchParam();
        articleSearchParam.setArticleId(articleId);
        articleSearchParam.setThemeCategoryId(0L);
        articleSearchParam.setUseArticleContent(useContent);
        articleSearchParam.setUseArticleMd(useMd);
        return webSearchMapper.selectArticleById(articleSearchParam);
    }

    /**
     * 
     * @param articleUrl
     * @param useMd
     * @param useContent
     * @return
     */
    public WebArticleVo selectArticleByUrl(String articleUrl,String themeName,Boolean useMd,Boolean useContent) {
        ArticleSearchParam articleSearchParam=new ArticleSearchParam();
        articleSearchParam.setArticleUrl(articleUrl);
        articleSearchParam.setThemeCategoryId(0L);
        articleSearchParam.setUseArticleContent(useContent);
        articleSearchParam.setUseArticleMd(useMd);
        return webSearchMapper.selectArticleById(articleSearchParam);
    }

    public List<WebArticleVo> listWebArticlesOrder(int num, int size, ArticleSearchParam ba, String order) {
        PageHelper.startPage(num, size, order);
        return this.listArticleBySearch(ba);
    }

    public List<WebArticleVo> listWebArticlesOrder(int num, int size, Integer articleType, Long catId, Long tagId,
            String themeName,
            OrderEnums order) {
        ArticleSearchParam bb = new ArticleSearchParam();
        if (!isSupportType(themeName)) {
            bb.setThemeCategoryId(Long.valueOf(configService.selectConfigDefaultValue(themeName, OlyWebConfigProperties.MAIN_ARTICLE_CATEGORY)));
        }
        bb.setArticleType(articleType);
        bb.setCategoryId(catId);
        return this.listWebArticlesOrder(num, size, bb, "article_top desc,create_time " + order.name());
    }

    public PageArticleTimeLine groupByTime(int pageNum, int pageSize, ArticleSearchParam bb) {
        PageHelper.startPage(pageNum, pageSize, "create_time desc");
        List<WebArticleVo> list = this.listArticleBySearch(bb);
        // 按照时间分组
        Map<String, List<WebArticleVo>> map = list.stream()
                .collect(Collectors.groupingBy(webArticle -> DateUtils.neData(webArticle.getCreateTime())));
        return PageArticleTimeLine.getData(list, map);
    }

    public PageArticleTimeLine groupByTime(int pageNum, int pageSize, String themeName, String crTime) {
        ArticleSearchParam bb = new ArticleSearchParam();
        bb.setCrTime(crTime);
        if (!isSupportType(themeName)) {
                       bb.setThemeCategoryId(Long.valueOf(configService.selectConfigDefaultValue(themeName, OlyWebConfigProperties.MAIN_ARTICLE_CATEGORY)));
        }
        return this.groupByTime(pageNum, pageSize, bb);
    }

    public boolean allowComment(long postId) {
        return webSearchMapper.allowComment(postId);
    }

    public int selectArticleNum(String themeName) {
        if (isSupportType(themeName)) {
            return webSearchMapper.selectArticleNum();
        }
        return webSearchMapper.selectArticleNumUnion(themeName);
    }

    /**
     * 自此类型是否为空
     * 主题名为空默认支持所有类型
     * 
     * @param themeName
     * @return
     */
    private boolean isSupportType(String themeName) {
        if (StringUtils.isEmpty(themeName)) {
            return true;
        } else {
            return StringUtils
                    .isEmpty(configService.selectConfigDefaultValue(themeName, OlyWebConfigProperties.ARTICLE_TYPES));
        }
    }

    public List<TimeNum> listArticleTimeNum(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return webSearchMapper.listArticleTimeNum();
    }
}
