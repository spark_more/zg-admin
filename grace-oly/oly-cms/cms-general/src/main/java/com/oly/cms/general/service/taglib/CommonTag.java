package com.oly.cms.general.service.taglib;

import org.springframework.stereotype.Service;

import com.grace.common.utils.StringUtils;

@Service("commonTag")
public class CommonTag {

    public String[] split(String s) {
        if (StringUtils.isEmpty(s.trim())) {
            return null;
        }
        return s.split(",");
    }
}
