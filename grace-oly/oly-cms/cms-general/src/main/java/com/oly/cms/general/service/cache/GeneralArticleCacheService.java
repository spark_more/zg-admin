package com.oly.cms.general.service.cache;

import java.util.List;

import com.oly.cms.common.constant.CmsCacheConstant;
import com.oly.cms.common.model.TimeNum;
import com.oly.cms.common.model.enums.OrderEnums;
import com.oly.cms.general.model.PageArticleTimeLine;
import com.oly.cms.general.service.search.GeneralArticleServiceImpl;
import com.oly.cms.query.model.param.ArticleSearchParam;
import com.oly.cms.query.model.vo.WebArticleVo;
import com.grace.common.core.text.Convert;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames = CmsCacheConstant.POST_CACHE_KEY_PREFIX)
public class GeneralArticleCacheService {

    @Autowired
    private GeneralArticleServiceImpl articleServiceImpl;

    @Cacheable(keyGenerator = "myKeyGenerator")
    public WebArticleVo selectWebArticleById(Long articleId,String themeName,Boolean useMd,Boolean useContent) {

        return articleServiceImpl.selectArticleById(articleId,themeName ,useMd, useContent);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public WebArticleVo selectWebArticleByUrl(String articleUrl,String themeName,Boolean useMd,Boolean useContent) {

        return articleServiceImpl.selectArticleByUrl(articleUrl,themeName , useMd, useContent);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public List<WebArticleVo> listWebArticlesOrder(int num, int size, Integer articleType, Long catId, Long tagId,
            String themeName,
            OrderEnums order) {
        return articleServiceImpl.listWebArticlesOrder(num, size, articleType, catId, tagId, themeName, order);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public PageArticleTimeLine groupByTime(int pageNum, int pageSize, String themeName, String crTime) {
        return articleServiceImpl.groupByTime(pageNum, pageSize, themeName, crTime);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public int selectArticleNum(String themeName) {
        return articleServiceImpl.selectArticleNum(themeName);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public List<TimeNum> listArticleTimeNum(int pageNum, int pageSize) {
        return articleServiceImpl.listArticleTimeNum(pageNum, pageSize);
    }

    /**
     * 综合查询
     * 
     * @param bb
     * @return
     */
    public List<WebArticleVo> listWebArticles(ArticleSearchParam bb) {

        return articleServiceImpl.listArticleBySearch(bb);
    }

    public boolean checkArticleSupportComment(String articleId) {
        return articleServiceImpl.allowComment(Convert.toLong(articleId));
    }

}
