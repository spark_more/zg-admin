package com.oly.cms.general.service.cache;

import java.util.List;

import com.grace.common.config.GraceConfig;
import com.oly.cms.common.constant.CmsCacheConstant;
import com.oly.cms.common.domain.entity.CmsCategory;
import com.oly.cms.common.enums.CategoryNodeTypeEnums;
import com.oly.cms.common.enums.CommonVisibleEnums;
import com.oly.cms.general.service.search.GeneralCategoryServiceImpl;
import com.oly.cms.query.model.param.CategorySearchParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames = CmsCacheConstant.CATS_CACHE_KEY_PREFIX)
public class GeneralCategoryCacheService {

    @Autowired
    private GeneralCategoryServiceImpl categoryServiceImpl;

    @Cacheable(keyGenerator = "myKeyGenerator")
    public CmsCategory selectCmsCategoryById(Long categoryId,String themeName) {
            CategorySearchParam param=new CategorySearchParam();
            param.setCategoryId(categoryId);
            param.setThemeName(GraceConfig.getOnlyCode()+"_"+themeName);
            param.setVisible(CommonVisibleEnums.SHOW.ordinal());
        return categoryServiceImpl.selectCmsCategoryById(param);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public List<CmsCategory> listCmsCategory(Long orderNum, CategoryNodeTypeEnums nodeType, Long categoryId,
            String themeName) {

        return categoryServiceImpl.listCmsCategory(orderNum, nodeType, categoryId,themeName);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public CmsCategory selectCmsCategoryTree(Long orderNum, CategoryNodeTypeEnums nodeType, Long categoryId,
            String themeName) {
        return categoryServiceImpl.selectCmsCategoryTree(orderNum, nodeType, categoryId, themeName);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public int selectCategoryNum(String themeName) {
        return categoryServiceImpl.selectCategoryNum(themeName);
    }

    public List<CmsCategory> listCmsCategory(CategorySearchParam param) {

        return categoryServiceImpl.listCmsCategory(param);
    }

    public CmsCategory selectCmsCategoryTree(CategorySearchParam param) {

        return categoryServiceImpl.selectCmsCategoryTree(param);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public boolean checkSupportCategory(String themeName, Long categoryId) {
        CategorySearchParam param=new CategorySearchParam();
            param.setCategoryId(categoryId);
            param.setThemeName(GraceConfig.getOnlyCode()+"_"+themeName);
            param.setVisible(CommonVisibleEnums.SHOW.ordinal());
        return categoryServiceImpl.checkSupportCategory(param);
    }

}
