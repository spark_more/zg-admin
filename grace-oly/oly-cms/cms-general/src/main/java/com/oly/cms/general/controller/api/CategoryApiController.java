package com.oly.cms.general.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oly.cms.common.constant.OlySystemConstant;
import com.oly.cms.common.enums.CategoryNodeTypeEnums;
import com.oly.cms.common.model.support.PageData;
import com.oly.cms.general.service.taglib.CategoryTag;
import com.oly.cms.query.model.param.CategorySearchParam;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;

@CrossOrigin
@RestController
@RequestMapping(OlySystemConstant.REQUEST_PREFIX+"/api/category")
public class CategoryApiController extends BaseController {

  @Autowired
  private CategoryTag categoryService;

  /**
   * 获取分类信息
   * 
   * @param themeName
   * @param categoryId
   * @return
   */
  @GetMapping("/{themeName}/get/{categoryId}")
  public AjaxResult selectCategoryById(@PathVariable("themeName") String themeName,@PathVariable("categoryId") Long categoryId) {
    
    return AjaxResult.success(categoryService.selectCmsCategoryById(categoryId,themeName));

  }

  /**
   * 获取分类数量
   * 
   * @param themeName
   * @return
   */
  @GetMapping("/{themeName}/getCategoryNum")
  public AjaxResult getCategoryNum(@PathVariable("themeName") String themeName) {
    return AjaxResult.success(categoryService.selectCategoryNum(themeName));
  }

  /**
   * 通过类型获取分类列表
   * @param themeName 主题名
   * @param type 类型
   * @param categoryId  上层节点categoryId
   * @return
   */
  @GetMapping("/{themeName}/list/nodeType/{nodeType}/{categoryId}")
  public AjaxResult listCmsCategoryByType(@PathVariable("themeName") String themeName,@PathVariable("nodeType") CategoryNodeTypeEnums nodeType, @PathVariable("categoryId") Long categoryId) {
    return AjaxResult.success(categoryService.listCategoryByType(themeName,nodeType, categoryId));
  }

  @GetMapping({"/{themeName}/list/orderNum/{orderNum}/{categoryId}"})
  public AjaxResult listCmsCategoryByOrderNum(@PathVariable("themeName") String themeName,@PathVariable("orderNum") long orderNum, @PathVariable("categoryId") long categoryId) {
    return AjaxResult.success(categoryService.listCategoryByOrderNum(themeName,orderNum, categoryId));
  }

  @GetMapping("/{themeName}/list/{categoryId}")
  public AjaxResult listCmsCategoryById(@PathVariable("categoryId") Long categoryId,
      @PathVariable(value = "themeName", required = true) String themeName) {
    return AjaxResult.success(categoryService.listCategoryById(categoryId, themeName));
  }

  @GetMapping("/{themeName}/tree/{categoryId}")
  public AjaxResult getCatgeoryTreeById(@PathVariable("categoryId") Long categoryId,
      @PathVariable(value = "themeName", required = false) String themeName) {

    return AjaxResult.success(categoryService.listCategoryById(categoryId, themeName));
  }

  @GetMapping({ "/list", "/list/themeName/{themeName}" })
  public AjaxResult listCategory(CategorySearchParam param,
      @PathVariable(value = "themeName", required = false) String themeName) {
    param.setSearchValue(themeName);
    PageData pageOne = PageData.getData(categoryService.listCmsCategory(param));
    return AjaxResult.success(pageOne);
  }

  @GetMapping("/tree")
  public AjaxResult getCategoryTree(CategorySearchParam param) {
    return AjaxResult.success(categoryService.getCmsCatsTree(param));
  }

}