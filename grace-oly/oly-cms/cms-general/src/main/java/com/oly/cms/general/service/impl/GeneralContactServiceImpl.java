package com.oly.cms.general.service.impl;

import java.util.List;

import com.oly.cms.common.constant.OlySystemConstant;
import com.oly.cms.common.domain.entity.CmsContact;
import com.oly.cms.general.service.IGeneralContactService;
import com.oly.cms.query.mapper.ContactSearchMapper;
import com.grace.common.utils.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 反馈|建议Service业务层处理
 * 
 * @author ZG
 * @date 2020-06-15
 */
@Service
public class GeneralContactServiceImpl implements IGeneralContactService {
    @Autowired
    private ContactSearchMapper contactSearchMapper;

    /**
     * 查询反馈|建议
     * 
     * @param contactId 反馈|建议ID
     * @return 反馈|建议
     */
    @Override
    public CmsContact selectCmsContactById(Long contactId) {
        return contactSearchMapper.selectCmsContactById(contactId);
    }

    /**
     * 查询反馈|建议列表
     * 
     * @param cmsContact 反馈|建议
     * @return 反馈|建议
     */
    @Override
    public List<CmsContact> selectCmsContactList(CmsContact cmsContact) {
        return contactSearchMapper.selectCmsContactList(cmsContact);
    }

    /**
     * 新增反馈|建议
     * 
     * @param cmsContact 反馈|建议
     * @return 结果
     */
    @Override
    public int insertCmsContact(CmsContact cmsContact) {
        if (StringUtils.isEmpty(cmsContact.getCreateBy())) {
            cmsContact.setCreateBy(OlySystemConstant.GUEST);
        }
        return contactSearchMapper.insertCmsContact(cmsContact);
    }

    /**
     * 修改反馈|建议
     * 
     * @param cmsContact 反馈|建议
     * @return 结果
     */
    @Override
    public int updateCmsContact(CmsContact cmsContact) {
        return contactSearchMapper.updateCmsContact(cmsContact);
    }

    /**
     * 删除反馈|建议对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCmsContactByIds(Long[] ids) {

        return contactSearchMapper.deleteCmsContactByIds(ids);
    }
}
