package com.oly.cms.general.service.taglib;

import java.util.List;

import com.oly.cms.common.model.TimeNum;
import com.oly.cms.common.model.enums.OrderEnums;
import com.oly.cms.common.model.support.PageData;
import com.oly.cms.general.model.PageArticleTimeLine;
import com.oly.cms.general.service.cache.GeneralArticleCacheService;
import com.oly.cms.query.model.param.ArticleSearchParam;
import com.oly.cms.query.model.vo.WebArticleVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 文章标签 涉及文章表的操作
 */
@Service("articleTag")
public class ArticleTag {
    @Autowired
    private GeneralArticleCacheService articleService;

    /**
     * 获取文章数量
     * 
     * @param themeName
     * @return
     */
    public int selectArticleNum(String themeName) {
        return articleService.selectArticleNum(themeName);
    }

    /**
     * 依据年月获取文章数量
     * 
     * @param pageNum
     * @param pageSize
     * @return
     */
    public List<TimeNum> listArticleTimeNum(int pageNum, int pageSize) {
        return articleService.listArticleTimeNum(pageNum, pageSize);
    }

    /**
     * 获取文章Html
     * 
     * @param articleId
     * @return
     */
    public WebArticleVo selectWebArticleHtmlById(Long articleId,String themeName) {
        return articleService.selectWebArticleById(articleId,themeName,false,true);
    }

    /**
     * 获取文章MarkDown
     * 
     * @param articleId
     * @return
     */
    public WebArticleVo selectWebArticleMdById(Long articleId,String themeName) {
        return articleService.selectWebArticleById(articleId,themeName,true,false);
    }

    /**
     * 获取文章Html
     * 
     * @param articleUrl
     * @return
     */
    public WebArticleVo selectWebArticleHtmlByUrl(String articleUrl,String themeName) {
       return articleService.selectWebArticleByUrl(articleUrl,themeName,false,true);
    }

    /**
     * 获取文章MarkDown
     * 
     * @param articleUrl
     * @return
     */
   public WebArticleVo selectWebArticleMdByUrl(String articleUrl,String themeName) {
       return articleService.selectWebArticleByUrl(articleUrl,themeName,true,false);
    }

    /**
     * 依据分类 严格模式
     * 
     * @param catId
     * @param num
     * @param size
     * @param themeName
     * @return
     */
    public List<WebArticleVo> listWebArticlesByCatId(long catId, int num, int size, String themeName) {

        return articleService.listWebArticlesOrder(num, size, null, catId, null, themeName, OrderEnums.DESC);
    }

    /**
     * 依据分类
     * 
     * @param catId
     * @param num
     * @param size
     * @return
     */
    public List<WebArticleVo> listWebArticlesByCatId(long catId, int num, int size) {

        return articleService.listWebArticlesOrder(num, size, null, catId, null, null, OrderEnums.DESC);
    }

    /**
     * 依据分类 严格模式
     * 
     * @param catId
     * @param themeName
     * @param num
     * @param size
     * @param order
     * @return
     */
    public List<WebArticleVo> listWebArticlesOrderByCatId(long catId, String themeName, int num, int size,
            OrderEnums order) {

        return articleService.listWebArticlesOrder(num, size, null, catId, null, themeName, order);
    }

    /**
     * 依据分类
     * 
     * @param catId
     * @param num
     * @param size
     * @param order
     * @return
     */
    public List<WebArticleVo> listWebArticlesOrderByCatId(long catId, int num, int size, OrderEnums order) {

        return articleService.listWebArticlesOrder(num, size, null, catId, null, null, order);
    }

    /**
     * 依据标签 严格模式
     * 
     * @param tagId
     * @param num
     * @param size
     * @param themeName
     * @return
     */
    public List<WebArticleVo> listWebArticlesByTagId(long tagId, int num, int size, String themeName) {

        return articleService.listWebArticlesOrder(num, size, null, null, tagId, themeName, OrderEnums.DESC);
    }

    /**
     * 依据标签
     * 
     * @param tagId
     * @param num
     * @param size
     * @return
     */
    public List<WebArticleVo> listWebArticlesByTagId(long tagId, int num, int size) {

        return articleService.listWebArticlesOrder(num, size, null, null, tagId, null, OrderEnums.DESC);
    }

    /**
     * 依据标签 严格模式
     * 
     * @param tagId
     * @param themeName
     * @param num
     * @param size
     * @param order
     * @return
     */
    public List<WebArticleVo> listWebArticlesOrderByTagId(long tagId, String themeName, int num, int size,
            OrderEnums order) {

        return articleService.listWebArticlesOrder(num, size, null, null, tagId, themeName, order);
    }

    /**
     * 依据标签
     * 
     * @param tagId
     * @param num
     * @param size
     * @param order
     * @return
     */
    public List<WebArticleVo> listWebArticlesOrderByTagId(long tagId, int num, int size, OrderEnums order) {

        return articleService.listWebArticlesOrder(num, size, null, null, tagId, null, order);
    }

    /**
     * 依据类型
     * 
     * @param articleType
     * @param pageNum
     * @param pageSize
     * @return
     */
    public List<WebArticleVo> listWebArticleByType(int articleType, int pageNum, int pageSize) {
        return articleService.listWebArticlesOrder(pageNum, pageSize, articleType, null, null, null, OrderEnums.DESC);
    }

    /**
     * 
     * @param articleType
     * @param pageNum
     * @param pageSize
     * @param order
     * @return
     */
    public List<WebArticleVo> listWebArticleByType(int articleType, int pageNum, int pageSize, OrderEnums order) {
        return articleService.listWebArticlesOrder(pageNum, pageSize, articleType, null, null, null, order);
    }

    /**
     * 文章列表
     * 
     * @param themeName
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageData pagePosts(String themeName, int pageNum, int pageSize) {
        List<WebArticleVo> list = articleService.listWebArticlesOrder(pageNum, pageSize, null, null, null, themeName,
                OrderEnums.DESC);
        PageData pageOne = PageData.getData(list);
        return pageOne;
    }

    /**
     * 文章列表通过标签ID
     * 
     * @param themeName
     * @param pageNum
     * @param pageSize
     * @param tagId
     * @return
     */
    public PageData pagePostsByTagId(String themeName, long tagId, int pageNum, int pageSize) {
        List<WebArticleVo> list = articleService.listWebArticlesOrder(pageNum, pageSize, null, null, tagId, themeName,
                OrderEnums.DESC);
        PageData pageOne = PageData.getData(list);
        return pageOne;
    }

    /**
     * 文章列表通过分类ID
     * 
     * @param themeName
     * @param pageNum
     * @param pageSize
     * @param catId
     * @return
     */
    public PageData pagePostsByCatId(String themeName, long catId, int pageNum, int pageSize) {
        List<WebArticleVo> list = articleService.listWebArticlesOrder(pageNum, pageSize, null, catId, null, themeName,
                OrderEnums.DESC);
        PageData pageOne = PageData.getData(list);
        return pageOne;
    }

    /**
     * 验证是文章否支持评论
     * 
     * @param articleId
     * @return
     */
    public boolean checkArticleSupportComment(String articleId) {
        return articleService.checkArticleSupportComment(articleId);
    }

    /**
     * 综合查询
     * 
     * @param bb
     * @return
     */
    public List<WebArticleVo> listWebArticles(ArticleSearchParam bb) {

        return articleService.listWebArticles(bb);
    }

    public PageArticleTimeLine groupByTime(int pageNum, int pageSize, String themeName, String crTime) {
        return articleService.groupByTime(pageNum, pageSize, themeName, crTime);
    }

}
