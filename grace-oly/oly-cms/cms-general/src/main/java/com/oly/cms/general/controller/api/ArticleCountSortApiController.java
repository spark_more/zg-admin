package com.oly.cms.general.controller.api;

import java.util.List;

import com.oly.cms.common.constant.OlySystemConstant;
import com.oly.cms.common.model.properties.OlyWebConfigProperties;
import com.oly.cms.common.model.support.PageData;
import com.oly.cms.general.service.cache.GeneralArticleVoCacheService;
import com.oly.cms.query.model.param.ArticleSearchParam;
import com.oly.cms.query.model.vo.WebArticleVo;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.utils.StringUtils;
import com.grace.system.service.impl.SysSearchConfigServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping(OlySystemConstant.REQUEST_PREFIX+"/api/articleSort")
public class ArticleCountSortApiController extends BaseController {

    @Autowired
    private GeneralArticleVoCacheService articleCountService;

    @Autowired
	private SysSearchConfigServiceImpl configService;

    @GetMapping({ "/list", "/{themeName}/list" })
    public AjaxResult list(@PathVariable(value = "themeName", required = false) String themeName,
            ArticleSearchParam bb) {
        if (StringUtils.isNotEmpty(themeName)) {
           bb.setThemeCategoryId(Long.valueOf(configService.selectConfigDefaultValue(themeName, OlyWebConfigProperties.MAIN_ARTICLE_CATEGORY)));
        }
        startPage();
        List<WebArticleVo> list = articleCountService.listArticleVo(bb);
        PageData pageOne = PageData.getData(list);
        return AjaxResult.success(pageOne);
    }

    @GetMapping("/getPreAndNextArticle/{themeName}/{articleId}")
    public AjaxResult selectPreAndNextArticle(@PathVariable(value = "themeName") String themeName,
            @PathVariable("articleId") long articleId) {
        return AjaxResult.success(articleCountService.selectPreAndNextArticle(articleId, themeName));
    }

}
