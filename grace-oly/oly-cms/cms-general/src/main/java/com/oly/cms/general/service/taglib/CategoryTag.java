package com.oly.cms.general.service.taglib;

import java.util.List;

import com.oly.cms.common.domain.entity.CmsCategory;
import com.oly.cms.common.enums.CategoryNodeTypeEnums;
import com.oly.cms.general.service.cache.GeneralCategoryCacheService;
import com.oly.cms.query.model.param.CategorySearchParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 分类标签
 */
@Service("categoryTag")
public class CategoryTag {

    @Autowired
    private GeneralCategoryCacheService categoryCacheService;

    /**
     * 获取分类
     * 
     * @param categoryId
     * @return
     */
    public CmsCategory selectCmsCategoryById(Long categoryId,String themeName) {
        return categoryCacheService.selectCmsCategoryById(categoryId,themeName);
    }

    /**
     * 
     * @param orderNum  排序
     * @param nodeType   类型
     * @param categoryId     节点ID
     * @param themeName 主题名字
     * @return
     */
    public List<CmsCategory> listCmsCategory(Long orderNum, CategoryNodeTypeEnums nodeType,Long categoryId,
            String themeName) {
        return categoryCacheService.listCmsCategory(orderNum, nodeType,categoryId,  themeName);
    }

    /**
     * 
     * @param nodeType   标签类型
     * @param categoryId     标签Id
     
     * @param themeName 主题名字
     * @return
     */
    public List<CmsCategory> listCmsCategory(CategoryNodeTypeEnums nodeType, Long categoryId, String themeName) {

        return categoryCacheService.listCmsCategory(null, nodeType,categoryId, themeName);
    }

    /**
     * 
     * @param orderNum
     * @param nodeType
     * @param parentId
     * @param categoryId
     * @param parent
     * @param themeName
     * @return
     */
    public CmsCategory selectCmsCategoryTree(Long orderNum, CategoryNodeTypeEnums nodeType, Long categoryId, 
            String themeName) {
        return categoryCacheService.selectCmsCategoryTree(orderNum, nodeType, categoryId,themeName);
    }

    /**
     * 分类树
     * 
     * @param categoryId
     * @param themeName
     * @return
     */
    public CmsCategory selectCmsCategoryTree(long categoryId, String themeName) {
        return categoryCacheService.selectCmsCategoryTree(null, null,  categoryId,themeName);
    }

    /**
     * 分类树
     * 
     * @param themeName
     * @return
     */
    public CmsCategory selectCmsCategoryByThemeName(String themeName) {
        return categoryCacheService.selectCmsCategoryTree(null, null,  null,themeName);
    }

    /**
     * 分类树
     * 
     * @param categoryId
     * @param themeName
     * @return
     */
    public CmsCategory selectCmsCategoryTree(long categoryId) {
        return categoryCacheService.selectCmsCategoryTree(null, null, categoryId,null);
    }

    /**
     * 通过分类id获取分类列表 包含自己及后代节点
     * 
     * @param categoryId
     * @param themeName 主题名字
     * @return
     */
    public List<CmsCategory> listCategoryById(long categoryId, String themeName) {
        return categoryCacheService.listCmsCategory(null, null,  categoryId,themeName);
    }

    /**
     * 儿子节点
     * 
     * @param categoryId
     * @param themeName 主题名字
     * @return
     */
    public List<CmsCategory> listCategoryByParentId(long parentId, String themeName) {
        return categoryCacheService.listCmsCategory(null, null, parentId, themeName);
    }

    /**
     * 
     * @param nodeType 类型
     * @param categoryId   分类Id
     * @return
     */
    public List<CmsCategory> listCategoryByType(String themeName,CategoryNodeTypeEnums nodeType, long categoryId) {
        return categoryCacheService.listCmsCategory(null, nodeType, categoryId, themeName);
    }

    /**
     * 
     * @param orderNum  排序
     * @param categoryId     分类Id
     * @param themeName 主题名字
     * @return
     */
    public List<CmsCategory> listCategoryByOrderNum(String themeName,Long orderNum, long categoryId) {
        return categoryCacheService.listCmsCategory(orderNum, null,  categoryId, themeName);
    }


    /**
     * @param themeName 主题名
     * @return
     */
    public List<CmsCategory> listCategoryByThemeName(String themeName) {
        return categoryCacheService.listCmsCategory(null, null, null, themeName);
    }

    /**
     * 获取数量
     * 
     * @param themeName
     * @return
     */
    public int selectCategoryNum(String themeName) {
        return categoryCacheService.selectCategoryNum(themeName);
    }

    public boolean checkSupportCat(String themeName, long categoryId) {
        return categoryCacheService.checkSupportCategory(themeName, categoryId);

    }

    public List<CmsCategory> listCmsCategory(CategorySearchParam param) {

        return categoryCacheService.listCmsCategory(param);
    }

    public CmsCategory getCmsCatsTree(CategorySearchParam param) {

        return categoryCacheService.selectCmsCategoryTree(param);
    }

}
