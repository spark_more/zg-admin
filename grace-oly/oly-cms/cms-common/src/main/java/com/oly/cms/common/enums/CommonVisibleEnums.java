
package com.oly.cms.common.enums;

/**
 * 链接,菜单，分类通用
 */
public enum CommonVisibleEnums {
    // 通过
    SHOW,
    // 隐藏/待处理
    HIDE,
    // 逻辑删除
    DELETE
}
