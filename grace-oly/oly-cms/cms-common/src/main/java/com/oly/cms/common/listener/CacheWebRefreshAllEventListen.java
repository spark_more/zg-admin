package com.oly.cms.common.listener;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.oly.cms.common.event.CacheWebRefreshAllEvent;

/**
 * 缓存刷新监听器
 * 
 * @author 止戈
 */
@Component
public class CacheWebRefreshAllEventListen {

  @Autowired
  private RedisTemplate<String, String> redisTemplate;

  @EventListener
  protected void handleVisitEvent(CacheWebRefreshAllEvent event) {
    Collection<String> cacheKeys = redisTemplate.keys(event.getKeyPrefix() + "*");
    redisTemplate.delete(cacheKeys);
  }

}
