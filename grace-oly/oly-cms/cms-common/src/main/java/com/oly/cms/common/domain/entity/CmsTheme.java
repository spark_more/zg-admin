package com.oly.cms.common.domain.entity;
import com.grace.common.annotation.Excel;
import com.grace.common.core.domain.BaseEntity;

/**
 * 主题对象 cms_theme
 * 
 * @author 止戈
 */
public class CmsTheme extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 站点ID */
    @Excel(name = "站点名字")
    private String webName;

    /** 主题名 */
    @Excel(name = "主题名")
    private String themeName;

    /** 站点别称 */
    @Excel(name = "站点别称")
    private String siteName;

    /** 主题作者 */
    @Excel(name = "主题作者")
    private String themeAuthor;

    /** 主题介绍 */
    @Excel(name = "主题介绍")
    private String themeInfo;

    /** 是否启用 */
    @Excel(name = "是否启用")
    private Integer themeEnabled;

    /** 更新地址 */
    @Excel(name = "更新地址")
    private String themeUpdate;

    /** 主题类型 */
    @Excel(name = "主题类型")
    private Integer themeType;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String themeTouch;

    /** 主题版本号 */
    @Excel(name = "主题版本号")
    private String themeVersion;

    public String getWebName() {
        return webName;
    }

    public void setWebName(String webName) {
        this.webName = webName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public String getThemeName() {
        return themeName;
    }

    public void setThemeAuthor(String themeAuthor) {
        this.themeAuthor = themeAuthor;
    }

    public String getThemeAuthor() {
        return themeAuthor;
    }

    public void setThemeInfo(String themeInfo) {
        this.themeInfo = themeInfo;
    }

    public String getThemeInfo() {
        return themeInfo;
    }

    public void setThemeEnabled(Integer themeEnabled) {
        this.themeEnabled = themeEnabled;
    }

    public Integer getThemeEnabled() {
        return themeEnabled;
    }

    public void setThemeUpdate(String themeUpdate) {
        this.themeUpdate = themeUpdate;
    }

    public String getThemeUpdate() {
        return themeUpdate;
    }

    public void setThemeTouch(String themeTouch) {
        this.themeTouch = themeTouch;
    }

    public String getThemeTouch() {
        return themeTouch;
    }

    public void setThemeVersion(String themeVersion) {
        this.themeVersion = themeVersion;
    }

    public String getThemeVersion() {
        return themeVersion;
    }

    public Integer getThemeType() {
        return themeType;
    }

    public void setThemeType(Integer themeType) {
        this.themeType = themeType;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    
    
}
