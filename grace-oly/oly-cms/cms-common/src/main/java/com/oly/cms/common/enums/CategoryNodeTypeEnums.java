package com.oly.cms.common.enums;

/**
 * 分类类型enum
 */
public enum CategoryNodeTypeEnums {
       //关联站点
       WEB_TRUCK(NodeTypeEnums.TRUNK),
       //关联主题    如果主题没有关联类别  默认为所有文章
       THEME_TRUCK(NodeTypeEnums.TRUNK),
       //类目节点  允许关联文章
       CATEGORY_TRUCK(NodeTypeEnums.TRUNK),
       //类目类型
       CATEGORY(NodeTypeEnums.LEAF),
       //标签节点  不允许关联文章
       TAG_TRUCK(NodeTypeEnums.TRUNK),
       //标签
       TAG(NodeTypeEnums.LEAF),
       //轮播节点  不允许关联文章
       CAROUSEL_TRUCK(NodeTypeEnums.TRUNK),
       //轮播
       CAROUSEL(NodeTypeEnums.LEAF),
       //推荐节点  不允许关联文章
       ONSHOW_TRUCK(NodeTypeEnums.TRUNK),
       //推荐
       ONSHOW(NodeTypeEnums.LEAF);
       
       private NodeTypeEnums nodeTypeEnum;
       CategoryNodeTypeEnums(NodeTypeEnums nodeTypeEnum){
           this.nodeTypeEnum=nodeTypeEnum;
       }
       public NodeTypeEnums getNodeTypeEnum() {
              return nodeTypeEnum;
       }

       public static CategoryNodeTypeEnums valueOf(int ordinal) {
              return values()[ordinal];
       }

       
    
}
