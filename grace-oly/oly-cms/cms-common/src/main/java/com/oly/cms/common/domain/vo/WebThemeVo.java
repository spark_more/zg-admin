package com.oly.cms.common.domain.vo;

import java.util.List;

import com.oly.cms.common.domain.entity.CmsWeb;

public class WebThemeVo extends CmsWeb {
    List<ThemeVo>  listTheme;

    public List<ThemeVo> getListTheme() {
        return listTheme;
    }

    public void setListTheme(List<ThemeVo> listTheme) {
        this.listTheme = listTheme;
    }
    
}
