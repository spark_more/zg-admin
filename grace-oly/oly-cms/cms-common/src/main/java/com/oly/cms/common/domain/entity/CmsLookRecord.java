package com.oly.cms.common.domain.entity;

import com.oly.cms.common.domain.base.BaseRecordModel;

/**
 * 阅读记录表
 * 
 * @author 止戈
 */
public class CmsLookRecord extends BaseRecordModel {

    private static final long serialVersionUID = 1L;
}
