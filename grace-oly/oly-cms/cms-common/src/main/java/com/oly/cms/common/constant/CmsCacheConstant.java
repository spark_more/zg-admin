package com.oly.cms.common.constant;

/**
 * 内容配置缓存配置前缀
 * 
 * @author 止戈
 */
public class CmsCacheConstant {

    /* 菜单缓存 */
    public final static String MENU_CACHE_KEY_PREFIX = "menu_cache:",
            /* 文章缓存 */
            POST_CACHE_KEY_PREFIX = "post_cache:",
            /**
             * 链接缓存
             */
            LINKS_CACHE_KEY_PREFIX = "links_cache:",

            /* 标签缓存 */
            TAGS_CACHE_KEY_PREFIX = "tags_cache:",
            /**
             * 分类缓存
             */
            CATS_CACHE_KEY_PREFIX = "cats_cache:",
            /**
             * 评论缓存
             */
            COMMENT_CACHE_KEY_PREFIX = "comment_cache:",
            /**
             * 联盟缓存
             */
            UNION_CACHE_KEY_PREFIX = "union_cache:";

}
