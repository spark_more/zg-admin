package com.oly.cms.common.enums;

public enum SearchVisibleType {
    //使用默认值 默认通过
    DEFAULT,
    //使用自定义状态
    USEVISIBLE,
    //使用这个时获取所有
    ALL
}
