package com.oly.cms.common.domain.entity;

import javax.validation.constraints.NotBlank;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import com.grace.common.annotation.Excel;
import com.grace.common.core.domain.BaseEntity;

/**
 * 站点对象 cms_web
 * 
 * @author 止戈
 */
public class CmsWeb extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 站点名 */
    @Excel(name = "站点名")
    private String webName;

    /** 管理Ip */
    @Excel(name = "管理Ip")
    private String manageIp;

    /** 站点地址 */
    @Excel(name = "站点地址")
    private String webUrl;
    
    /** 是否启动 */
    @Excel(name = "是否启动")
    private Integer startNow;

    public void setWebName(String webName) {
        this.webName = webName;
    }

    @NotBlank(message = "站点名不能为空")
    @Length(min = 2, max = 8, message = "站点限制2到8个字符")
    public String getWebName() {
        return webName;
    }

    public String getManageIp() {
        return manageIp;
    }

    public void setManageIp(String manageIp) {
        this.manageIp = manageIp;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public Integer getStartNow() {
        return startNow;
    }

    public void setStartNow(Integer startNow) {
        this.startNow = startNow;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("webName", getWebName())
                .append("webUrl", getWebUrl())
                .append("manageIp", getManageIp())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("updateBy", getUpdateBy())
                .append("createBy", getCreateBy())
                .append("remark", getRemark())
                .append("startNow", getStartNow())
                .toString();
    }
}
