package com.oly.cms.common.constant;

/**
 * 内容配置前缀
 * 
 * @author 止戈
 */
public class OlySystemConstant {
    // 默认顶置
    public static int DEFAULT_POST_TOP = 0;
    
    // controller 请求前缀
    public final static String REQUEST_PREFIX="/web";

    /**
     * 游客
     */
    public static final String GUEST = "guest";

    // 新建主题文件支持类型
    public static String[] THEME_SUPPORT_PREFIX = { "html", "json", "yaml", "text", "js", "css" };

}
