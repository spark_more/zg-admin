package com.oly.cms.common.domain.base;

import com.grace.common.annotation.Excel;

/**
 * Seo基类
 * 导航Menu
 * 类目Category
 * 文章Article
 * @author 止戈
 */
public class SeoBaseModel extends BaseModel {
    private static final long serialVersionUID = 1L;
    /** 关键词 */
    @Excel(name = "关键词")
    private String keywords;

    /** 描述 */
    @Excel(name = "描述")
    private String description;
    
    /** 页面类型 页面风格选择 */
    @Excel(name = "页面类型")
    private Integer pageType;

    public Integer getPageType() {
        return pageType;
    }

    public void setPageType(Integer pageType) {
        this.pageType = pageType;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
