package com.grace.common.config;

import java.nio.file.Paths;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取项目相关配置
 * 
 * @author grace
 */
@Component
@ConfigurationProperties(prefix = "grace")
public class GraceConfig {
    /** 项目名称 */
    private String name;

    /** 版本 */
    private String version;

    /** 版权年份 */
    private String copyrightYear;

    /** 是否指定任意目录 */
    private static boolean appointProfile;

    /** 工作路径 */
    private static String profile;

    /** 获取地址开关 */
    private static boolean addressEnabled;

    /** 验证码类型 */
    private static String captchaType;

    /** 唯一编码 */
    private static String onlyCode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCopyrightYear() {
        return copyrightYear;
    }

    public void setCopyrightYear(String copyrightYear) {
        this.copyrightYear = copyrightYear;
    }

    public static boolean isAppointProfile() {
        return appointProfile;
    }

    public static void setAppointProfile(boolean appointProfile) {
        GraceConfig.appointProfile = appointProfile;
    }

    public static String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        if (appointProfile) {
            GraceConfig.profile = profile;
        } else {
            GraceConfig.profile = Paths.get(getUserWork(), profile).toString();
        }
    }

    public static boolean isAddressEnabled() {
        return addressEnabled;
    }

    public void setAddressEnabled(boolean addressEnabled) {
        GraceConfig.addressEnabled = addressEnabled;
    }

    public static String getCaptchaType() {
        return captchaType;
    }

    public void setCaptchaType(String captchaType) {
        GraceConfig.captchaType = captchaType;
    }

    /**
     * 获取导入上传路径
     */
    public static String getImportPath() {
        return getProfile() + "/import";
    }

    /**
     * 获取头像上传路径
     */
    public static String getAvatarPath() {
        return getProfile() + "/avatar";
    }

    /**
     * 获取下载路径
     */
    public static String getDownloadPath() {
        return getProfile() + "/download/";
    }

    /**
     * 获取上传路径
     */
    public static String getUploadPath() {
        return getProfile() + "/upload";
    }

    /**
     * 用户工作空间
     * 
     * @return
     */
    public static String getUserWork() {
        return System.getProperty("user.home");
    }

    /**
     * 获取设备唯一编码
     * 
     * @return
     */
    public static String getOnlyCode() {
        return onlyCode;
    }

    public void setOnlyCode(String onlyCode) {
        GraceConfig.onlyCode = onlyCode;
    }

}
