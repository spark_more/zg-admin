package com.grace.common.enums;

/**
 * 数据源
 * 
 * @author grace
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
