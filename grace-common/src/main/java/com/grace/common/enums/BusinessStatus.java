package com.grace.common.enums;

/**
 * 操作状态
 * 
 * @author grace
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
