package com.grace.system.service;

import java.util.List;
import java.util.Map;

import com.grace.common.constant.CacheConstants;
import com.grace.common.properties.PropertyEnum;
import com.grace.system.domain.SysConfig;

/**
 * 参数配置 数据层
 * 
 * @author zg
 */
public interface ISysSearchConfigService {
  /**
   * 查询参数配置信息
   * 
   * @param config 查询参数
   * @return 参数配置信息
   */
  public SysConfig selectConfig(SysConfig sysConfig);

  /**
   * 查询参数配置信息
   * 
   * @param configId 参数配置ID
   * @return 参数配置信息
   */
  public SysConfig selectConfigById(Long configId);

  /**
   * 查询参数配置信息
   * 
   * @param configGroup 参数组
   * @param configKey   参数键名
   * @return 参数配置信息
   */
  public SysConfig selectConfigByGk(String configGroup, String configKey);

  /**
   * 根据Gk查询参数配置信息
   * 
   * @param configGroup 参数组
   * @param configKey   参数键名
   * @return 参数键值
   */
  public String selectConfigValueByGk(String configGroup, String configKey);

  /**
   * 查询参数配置列表
   * 
   * @param config 参数配置信息
   * @return 参数配置集合
   */
  public List<SysConfig> selectConfigList(SysConfig config);

  /**
   * 获取配置列表
   * 
   * @return 配置组列表
   */
  public List<String> selectConfigGroupList();

  /**
   * 查询参数配置列表转Map
   * key config
   * 
   * @param config 参数配置信息
   * @return 参数配置集合
   */
  public Map<String, SysConfig> selectConfigMap(SysConfig config);

  /**
   * 查询参数配置列表转Map
   * key value
   * 
   * @param config 参数配置信息
   * @return 参数配置集合
   */
  public Map<String, String> selectConfigValueMap(SysConfig config);

  /**
   * 查询参数配置列表转Map
   * 
   * @param configGroup 参数配置组
   * @return 参数配置集合
   */
  public Map<String, SysConfig> selectConfigMapByGroupName(String configGroup);

  /**
   * 查询参数配置列表转Map
   * string string
   * 
   * @param configGroup 参数配置组
   * @return 参数配置集合
   */
  public Map<String, String> selectConfigValueMapByGroupName(String configGroup);

  /**
   * 校验参数键名是否唯一
   * 
   * @param config 参数信息
   * @return 结果
   */
  public boolean checkConfigGkUnique(SysConfig config);

  /**
   * 如果获取到的信息为空 返回默认值
   * 
   * @param propertyEnum
   * @return
   */
  public String selectConfigDefaultValue(String groupName, PropertyEnum propertyEnum);

  /**
   * 设置cache key
   * 
   * @param configGroup 参数组
   * @param configKey   参数键
   * @return 缓存键key
   */
  public static String getCacheKey(String configGroup, String configKey) {
    return CacheConstants.SYS_CONFIG_KEY + configGroup + "_" + configKey;
  }

  /**
   * 查询参数转化为
   * string obj
   * 
   * @param configGroup
   * @return
   */
  public Map<String, Object> selectConfigValueObjMap(String configGroup);

  /**
   * 获取验证码开关
   * 
   * @param configGroup
   * @return true开启，false关闭
   */
  public boolean selectCaptchaEnabled(String groupName);

}
