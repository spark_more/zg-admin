package com.grace.system.mapper;

import java.util.List;
import com.grace.system.domain.SysHoldData;

/**
 * 资源数据映射Mapper接口
 * 
 * @author grace
 * @date 2023-09-07
 */
public interface SysHoldDataMapper 
{
    /**
     * 查询资源数据映射
     * 
     * @param sysHoldData
     * @return 资源数据映射
     */
    public SysHoldData selectSysHoldData(SysHoldData sysHoldData);

    /**
     * 查询资源数据映射列表
     * 
     * @param sysHoldData 资源数据映射
     * @return 资源数据映射集合
     */
    public List<SysHoldData> selectSysHoldDataList(SysHoldData sysHoldData);

    /**
     * 新增资源数据映射
     * 
     * @param sysHoldData 资源数据映射
     * @return 结果
     */
    public int insertSysHoldData(SysHoldData sysHoldData);

    /**
     * 修改资源数据映射
     * 
     * @param sysHoldData 资源数据映射
     * @return 结果
     */
    public int updateSysHoldData(SysHoldData sysHoldData);

   /**
    * 删除
    * @param sysHoldData
    * @return 结果
    */
    public int deleteSysHoldData(SysHoldData sysHoldData);
}
