package com.grace.system.service;

import java.util.Map;

import com.grace.system.domain.SysConfig;

/**
 * 参数配置 服务层
 * 
 * @author zg
 */
public interface ISysConfigService extends ISysSearchConfigService {

    /**
     * 新增参数配置
     * 
     * @param config 参数配置信息
     * @return 结果
     */
    public int insertConfig(SysConfig config);

    /**
     * 修改参数配置
     * 
     * @param config 参数配置信息
     * @return 结果
     */
    public int updateConfigById(SysConfig config);

    /**
     * 修改参数配置ByGk
     * 
     * @param config 参数配置信息
     * @return 结果
     */
    public int updateConfigByGk(SysConfig config);

    /**
     * 批量修改参数配置
     * 
     * @param map   参数配置
     * @param login 登录用户
     * @return 结果
     */
    public int updatesByMap(Map<String, Object> map, String login);

    /**
     * 批量删除参数信息
     * 
     * @param configIds 需要删除的参数ID数组
     */
    public void deleteConfigByIds(Long[] configIds);

    /**
     * 删除参数配置对象ByGk
     * 
     * @param config
     * @return 结果
     */
    public int deleteConfigByGk(String configGroup, String configKey);

    /**
     * 删除参数配置
     * 
     * @param configGroup 参数配置组
     * @return 结果
     */
    public int deleteConfigByGroup(String configGroup);

    /**
     * 加载参数缓存数据
     */
    public void loadingConfigCache();

    /**
     * 清空参数缓存数据
     */
    public void clearConfigCache();

    /**
     * 重置参数缓存数据
     */
    public void resetConfigCache();

}
