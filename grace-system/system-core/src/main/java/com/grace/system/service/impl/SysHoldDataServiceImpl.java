package com.grace.system.service.impl;

import java.util.List;
import com.grace.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.grace.system.mapper.SysHoldDataMapper;
import com.grace.system.domain.SysHoldData;
import com.grace.system.service.ISysHoldDataService;

/**
 * 资源数据映射Service业务层处理
 * 
 * @author grace
 * @date 2023-09-07
 */
@Service
public class SysHoldDataServiceImpl implements ISysHoldDataService 
{
    @Autowired
    private SysHoldDataMapper sysHoldDataMapper;

    /**
     * 查询资源数据映射
     * 
     * @param sysHoldData
     * @return 资源数据映射
     */
    @Override
    public SysHoldData selectSysHoldData(SysHoldData sysHoldData)
    {
        return sysHoldDataMapper.selectSysHoldData(sysHoldData);
    }

    /**
     * 查询资源数据映射列表
     * 
     * @param sysHoldData 资源数据映射
     * @return 资源数据映射
     */
    @Override
    public List<SysHoldData> selectSysHoldDataList(SysHoldData sysHoldData)
    {
        return sysHoldDataMapper.selectSysHoldDataList(sysHoldData);
    }

    /**
     * 新增资源数据映射
     * 
     * @param sysHoldData 资源数据映射
     * @return 结果
     */
    @Override
    public int insertSysHoldData(SysHoldData sysHoldData)
    {
        sysHoldData.setCreateTime(DateUtils.getNowDate());
        return sysHoldDataMapper.insertSysHoldData(sysHoldData);
    }

    /**
     * 修改资源数据映射
     * 
     * @param sysHoldData 资源数据映射
     * @return 结果
     */
    @Override
    public int updateSysHoldData(SysHoldData sysHoldData)
    {
        sysHoldData.setUpdateTime(DateUtils.getNowDate());
        return sysHoldDataMapper.updateSysHoldData(sysHoldData);
    }

    @Override
    public int deleteSysHoldData(SysHoldData sysHoldData) {
       
        return sysHoldDataMapper.deleteSysHoldData(sysHoldData);
    }

}
